/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* Transparent simple counter instead of real clock.
 * Then tracing under an OS, clock_gettime requires a sys-call which spoils
 * extreme performance.
 *
 * For when actual time is not important, rather performance and order is:
 *
 * - race-validation of tracing multithreaded programs
 * - Performance impact is a concern
 *
 * */

#include <stdint.h>

static uint64_t clock_cntr = 0;

uint64_t utrace_clock()
{
    return clock_cntr++;
}
