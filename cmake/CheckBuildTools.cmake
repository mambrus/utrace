#
# Tools nessesary for build
#
find_program(EXE_HSUBST
    hsubst
)
if(EXE_HSUBST STREQUAL "EXE_HSUBST-NOTFOUND")
    message(FATAL_ERROR "Please install hsubst")
endif()

find_program(EXE_LDOC
    ldoc
)
if(EXE_LDOC STREQUAL "EXE_LDOC-NOTFOUND")
    message(FATAL_ERROR "Please install ldoc")
endif()


#
# Local tools
#
#find_program(EXE_SIGGEN
#    siggen
#    PATHS ${CMAKE_SOURCE_DIR}/bin
#)
#if(EXE_SLOG STREQUAL "EXE_SIGGEN-NOTFOUND")
#    message(FATAL_ERROR "Please install siggen (signal handler code-gen)")
#endif()
