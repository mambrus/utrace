
set(SYSROOT
    ${CMAKE_SYSROOT}
    CACHE STRING
    "System path (--system=)")
mark_as_advanced(SYSROOT)

if (NOT SYSROOT STREQUAL "")
    message(STATUS "** INFO: SYSROOT was either set or defaulted from toolchain file" )
    set(CMAKE_SYSROOT "${SYSROOT}")
    add_compile_options(--sysroot=${CMAKE_SYSROOT})
endif ()
