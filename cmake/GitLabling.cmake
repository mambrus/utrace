#
# SHA1/Tags from git-log
#
find_program(EXE_GIT git)
if(EXE_GIT STREQUAL "GIT-NOTFOUND")
    message(FATAL_ERROR "Please install git")
endif()
find_package(Git)
mark_as_advanced(EXE_GIT)

find_program(EXE_TAIL tail)
if(EXE_TAIL STREQUAL "TAIL-NOTFOUND")
    message(FATAL_ERROR "Please install tail")
endif()
mark_as_advanced(EXE_TAIL)

find_program(EXE_SED sed)
if(EXE_SED STREQUAL "SED-NOTFOUND")
    message(FATAL_ERROR "Please install sed")
endif()
mark_as_advanced(EXE_SED)

#
# Deduct SHA1 from git-log history. Append the string "-dirty" if not
# commited yet
#
execute_process(COMMAND
    "${GIT_EXECUTABLE}" describe --match=NeVeRmAtCh --always --abbrev=7 --dirty
    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
    OUTPUT_VARIABLE GIT_SHA1
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)

#
# Last tag in history
#
execute_process(
    COMMAND "${GIT_EXECUTABLE}" tag
    COMMAND "${EXE_TAIL}" -n1
    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
    OUTPUT_VARIABLE GIT_TAG
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)

#
# Release deducted from last tag in history and stript until the last
#
execute_process(
    COMMAND "${GIT_EXECUTABLE}" tag
    COMMAND "${EXE_TAIL}" -n1
    COMMAND "${EXE_SED}" -Ee s/.*-//
    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
    OUTPUT_VARIABLE GIT_RELEASE
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)

message(STATUS "GIT_SHA1: ${GIT_SHA1}")
message(STATUS "GIT_TAG: ${GIT_TAG}")
message(STATUS "GIT_RELEASE: ${GIT_RELEASE}")

