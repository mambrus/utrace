include(CheckLibraryExists)
include(CheckTypeSize)
include(CheckCSourceCompiles)
include(CheckIncludeFiles)
include(CheckFunctionExists)
include(CheckSymbolExists)
include(GNUInstallDirs)

CHECK_LIBRARY_EXISTS(rt clock_gettime "" HAVE_LIB_RT)
CHECK_LIBRARY_EXISTS(pthread pthread_create "" HAVE_LIB_PTHREAD)

if (HAVE_LIB_RT)
    set(EXTRA_LIBS ${EXTRA_LIBS} rt)
endif ()
if (HAVE_LIB_PTHREAD)
    set(EXTRA_LIBS ${EXTRA_LIBS} pthread)
endif ()

CHECK_LIBRARY_EXISTS(sotcptap init_server "" HAVE_LIB_TCPTAP_SHARED)
if (HAVE_LIB_TCPTAP_SHARED)
    set(LIBS_SO ${LIBS_SO} sotcptap)
else ()
    message (WARNING
        "TCP-TAP (so) not found. Will not build what's dependent of TCP-TAP (so)")
endif ()

FIND_LIBRARY(HAVE_LIB_TCPTAP_STATIC atcptap)
FIND_LIBRARY(HAVE_LIB_LIBLOG_STATIC liblog_static)
CHECK_LIBRARY_EXISTS(liblog log_write "/usr/local/lib/" HAVE_LIB_LIBLOG_SHARED)
if (HAVE_LIB_TCPTAP_STATIC AND HAVE_LIB_LIBLOG_STATIC)
    set(LIBS_A ${LIBS_A} atcptap)
    set(LIBS_A ${LIBS_A} liblog_static)
else ()
    message (WARNING
        "TCP-TAP (a) not found. Will not build what's dependent of TCP-TAP (a)")
endif ()
if (HAVE_LIB_LIBLOG_SHARED OR HAVE_LIB_LIBLOG_STATIC)
    set(HAVE_LIB_LIBLOG ON)
endif ()

FIND_LIBRARY(HAVE_LIB_XB_LUACONSOLE xb_luaconsole)
FIND_LIBRARY(HAVE_LIB_XB_LUAAUX xb_luaaux)
FIND_LIBRARY(HAVE_LIB_XB_LUACORE xb_luacore)
FIND_LIBRARY(HAVE_LIB_XB_LUALIB xb_lualib)

if (HAVE_LIB_XB_LUACONSOLE AND
        HAVE_LIB_XB_LUAAUX AND
        HAVE_LIB_XB_LUACORE AND
        HAVE_LIB_XB_LUALIB)

    set(LIBS_SO ${LIBS_SO} xb_luaconsole)
    set(LIBS_SO ${LIBS_SO} xb_luaaux)
    set(LIBS_SO ${LIBS_SO} xb_luacore)
    set(LIBS_SO ${LIBS_SO} xb_lualib)
    set(XB_LUA_DETECT ON)
mark_as_advanced(UTRACE_HAS_XB_LUA)
else ()
    message (WARNING
        "XB_LUA (so) not found. Will not build/use what's dependent of XB_LUA (so)")
endif ()


FIND_LIBRARY(HAVE_LIB_XB_LUACONSOLE_STATIC xb_luaconsole_static)
FIND_LIBRARY(HAVE_LIB_XB_LUAAUX_STATIC xb_luaaux_static)
FIND_LIBRARY(HAVE_LIB_XB_LUACORE_STATIC xb_luacore_static)
FIND_LIBRARY(HAVE_LIB_XB_LUALIB_STATIC xb_lualib_static)

if (HAVE_LIB_XB_LUACONSOLE_STATIC AND
        HAVE_LIB_XB_LUAAUX_STATIC AND
        HAVE_LIB_XB_LUACORE_STATIC AND
        HAVE_LIB_XB_LUALIB_STATIC)

    set(LIBS_A ${LIBS_A} xb_luaconsole_static)
    set(LIBS_A ${LIBS_A} xb_luaaux_static)
    set(LIBS_A ${LIBS_A} xb_luacore_static)
    set(LIBS_A ${LIBS_A} xb_lualib_static)
    set(XB_LUA_DETECT ON)
else ()
    message (WARNING
        "XB_LUA (a) not found. Will not build/use what's dependent of XB_LUA (a)")
endif ()

set(UTRACE_HAS_XB_LUA
    ${XB_LUA_DETECT}
    CACHE BOOL
    "xb-lua exists on system (autodeted overloadable)")
mark_as_advanced(UTRACE_HAS_XB_LUA)

if (UTRACE_HAS_XB_LUA)
    add_compile_options(-D HAS_XB_LUA)
endif ()
