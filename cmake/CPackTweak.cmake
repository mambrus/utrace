#
# CPack pre 3.15.5 ignores CPACK_PACKAGE_VERSION_TWEAK
#
# --- include this file...
#   BEFORE include(CPack)
#   AFTER  include(GitLabling)
#---
#
# Replicates same algo as in /usr/share/cmake-3.16/Modules/CPack.cmake
# but append CPACK_PACKAGE_VERSION_TWEAK if it exists
#
# NOTE:
#   * CPACK_PACKAGE_VERSION_TWEAK Does not need to be a number
#   * It's up to the package maintainer to make sure the package gets
#     considered in ascending order by each Distributions packet management
#     system
#   * Safest bet is however to use a 4'th digit to PROJECT(VERSION ....)
#     and have CMake assert it's a number

set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION_MAJOR}")
#string(REGEX REPLACE [^0-9.] "" TAG_NR "${GIT_TAG}") ## Numerical value from tag
if(CPACK_PACKAGE_VERSION_MINOR GREATER_EQUAL 0)
    string(APPEND CPACK_PACKAGE_VERSION ".${CPACK_PACKAGE_VERSION_MINOR}")
    if(CPACK_PACKAGE_VERSION_PATCH GREATER_EQUAL 0)
        string(APPEND CPACK_PACKAGE_VERSION ".${CPACK_PACKAGE_VERSION_PATCH}")
    endif()
    if(CPACK_PACKAGE_VERSION_TWEAK)
        string(APPEND CPACK_PACKAGE_VERSION "-${CPACK_PACKAGE_VERSION_TWEAK}")
    elseif(GIT_SHA1)
        string(APPEND CPACK_PACKAGE_VERSION "-${GIT_SHA1}")
    endif()
endif()
