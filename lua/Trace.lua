--[[--
Declare a new class "Trace" into Lua global table

This class is a helper-class to manage low-level `probes` and `tracing` in a
user friendlier way. The fundamental friendliness is based of the concept of
`selections` (see method `sel` below).

@see utrace-probe
@see examples-Trace
@classmod Trace
--]]

-- Declare a new empty table to become a class
Trace = {};

setmetatable(Trace, {
    __name = "class.Trace",
    __index = table,
    __call = function(_, ...)
        local arg1 = ...

        if arg1 then
            return Trace:new(...)
        else
            return setmetatable({...}, Trace.mt)
        end
    end
    }
)

--[[--
Assisting Meta-table

Operators

@table mt
--]]
Trace.mt = {
    __index = Trace,
    __tostring = function(self)
        if #self == 0 then
            -- May be "class" itself, print it's version rely on any other
            -- use of instance of 0 elements to crash in run-time
            local utrace = self:version();
            return "{" .. utrace .. "}"
        else
            -- Content as pretty-format string
            local s = "{"
            for i, l in ipairs(self) do
                if i == 1 then
                    s = s .. "\n"
                else
                    s = s .. ",\n"
                end
                s = s .. "  {" .. l.ptr
                s = s .. ",  " .. "\"" .. l.kind .. "\""
                s = s .. ",  " .. "\"" .. l. filename  .. "\""
                s = s .. ",  " .. "\"" .. l.funcname  .. "\""
                s = s .. ",  " .. l.line .. "}"
            end
            s = s .. "\n}"
            return s;
        end
    end,
    __add = function(self, B)
        -- operator `+`: Join two Trace selections and return result into a new
        -- 3'rd
        if type(B) == "table" then
            local r = self:new()
            for i,l in ipairs(self) do table.insert(r, l) end
            -- Join tables into a new  selection 'r' leaving 'self` intact
            for i,l in ipairs(B) do table.insert(r, l) end
            return r
        else
            error("Invalid type of RHS for + operator. Can only join two Trace objects")
        end
    end;
}

--[[--
version

Returns (1) version string

@usage
Trace:version()
-- utrace <0.3.4-8269c16>

@function version
@treturn String version
--]]
function Trace:version(...)
    return probe.version() -- probe/trace API share the same version
end

--[[--
listall

Prints to console a list of all probes. Note that LOG-probes are included as
even if a LOG is muted/inactive, it's still also a trace-point "probe".

@function listall
@treturn Integer Total number of probes
--]]
function Trace:listall(...)
    local s = Trace:sel()
    for i, l in ipairs(s) do
        print(i, l.ptr, l.kind, l.filename, l.funcname, l.line)
    end

    return n;
end

--[[--
p_sel

Print a selection. Usually for interactive use before acting upon it to make
sure the conditions are covering the intended probe/probes.

Normally used interactively to determine enough specific criteria for _a_
probe", not _several_ probes.

Note that the function is not returning the list for Lua-VM. It's printing
to console for either interactive use or for RPC use.

Print is a columns of: ID, kind, filename, funcname, line

Arguments are the same as `sel` (follow "See also") OR one Trace instance
with a selection.

@see sel
@function p_sel
@tparam[opt] String filename -OR- Trace instance
@tparam[opt] String funcname
@tparam[opt] String kind
@treturn Integer Number of probes fitting selection
--]]
function Trace:p_sel(...)
    local arg1, funcname, kind = ...
    local t
    if type(arg1) == "table" then
        t = arg1
    else
        t = Trace:sel(...)
    end
    local j = 0 -- Number of probes accessed
    for i, l in ipairs(t) do
        if (t[i].ptr) then
            j = j + 1
            print(j, t[i].ptr, t[i].kind, t[i].filename, t[i].funcname, t[i].line)
        end
    end
    return j
end

--[[--
p_stat

Print status for a selection of probes. This is handy for determining if one
or several probes are live or not. I.e. it's "passed-counters" and "latest
time-stamps" are changing.

The list is the same as `p_sel` with regards to order order and ID, just that
the 3 last columns "file", "function" and "line" are replaced with
"control", "counter" and "time-stamp".

Note that the function is not returning the list for Lua-VM. It's printing
to console for either interactive use or for RPC use.

Print is a columns of: ID ,kind ,filename ,func-name ,line <i>[,d-time
,d-counts ,linked-probe-ID]</i>

<b>Note</b> that the last three columns are conditional and depends of if
probe is diff-event linked to another or not (see `utrace-probe.setdprobe`)

Arguments are the same as `sel` (follow "See also") OR one Trace instance
with a selection.

@see sel
@function p_stat
@tparam[opt] String filename -OR- Trace instance
@tparam[opt] String funcname
@tparam[opt] String kind
@treturn Integer Number of probes fitting selection
--]]
function Trace:p_stat(...)
    local t
    local arg1, funcname, kind
    local args = {...}

    if #args > 0 then
        arg1, funcname, kind = ...
    else
        arg1 = self
    end

    if type(arg1) == "table" then
        t = arg1
    else
        t = Trace:sel(...)
    end
    local j = 0 -- Number of probes accessed
    for i, l in ipairs(t) do
        if (l.ptr) then
            j = j + 1
            -- Get probes current:
            -- ctrl     : Control setting (logging, tracing, off etc)
            -- cnt      : Passed counter
            -- ts       : Last passed time-stamp
            -- diff_t   : Time-delta against diff_p
            -- diff_n   : Counter-delta against diff_p
            -- diff_p   : Linked diff-probe ID
            local ctrl, cntr, ts, diff_p, diff_t, diff_n = probe.getdata(l.ptr)
            if diff_p then
                print(j, l.ptr, l.kind, ctrl, cntr, ts,
                    diff_t, diff_n, diff_p)
            else
                print(j, l.ptr, l.kind, ctrl, cntr, ts)
            end
        end
    end
    return j
end

--[[--
stat

Return a table of status for a selection of probes. Identical to `p_stat`
except that nothing is printed and the return is a `table`. Note that the
table is not live, i.e. Function needs to be re-iterated for current values.

@usage
a = Trace('test_main.c','thread_A',nil,1)
b = Trace('test_main.c','thread_A',nil,2)
Trace:set(a + b, 1)   -- 2
Trace:dprobe(b, a)    -- Establish relation
t = Trace:stat(a + b) -- Table with probes and stats
print("Time between probes: " .. t[2][5]/1E6 .. " (mS)")
-- Time between probes: 5.05918 (mS)

@see new
@see dprobe
@see p_stat
@function stat
@tparam[opt] String filename -OR- Trace instance
@tparam[opt] String funcname
@tparam[opt] String kind
@treturn Table of statuses
--]]
function Trace:stat(...)
    local t
    local r = {}
    local arg1, funcname, kind
    local args = {...}

    if #args > 0 then
        arg1, funcname, kind = ...
    else
        arg1 = self
    end
    if type(arg1) == "table" then
        t = arg1
    else
        t = Trace:sel(arg1, funcname, kind)
    end
    for i, l in ipairs(t) do
        if (l.ptr) then
            local e = { probe.getdata(l.ptr) }
            table.insert(r, e)
        end
    end
    return r
end

--[[--
whatis

Look-up code reference for a certain probe-ID. Note: not selection (`sel`)
based but ID-specific. Useful after determining which probe is live `p_stat`
like in the example below.

@usage
-- Enable probes of selection defined by arguments
Trace:set({Trace:sel("test_main.c","thread_A")}, 1)
-- 1       94651015758336  T       1       0       0
-- 2       94651015758400  T       1       0       0

-- Print statuses of selection defined by arguments
Trace:p_stat("test_main.c")
-- 1       94376905458432  T       0       0       0
-- 2       94376905458496  T       0       0       0
-- 3       94376905458304  T       0       0       0
-- 4       94376905458368  T       0       0       0
-- 5       94376905458176  T       1       405     96874004698502
-- 6       94376905458240  T       1       406     96874009765001

-- Look-up one of the probes from the list above
Trace:whatis(94376905458240)
-- T       test_main.c     thread_A        59

@function whatis
@tparam Integer Probe-ID (address)
@treturn String kind What kind of probe is it (T="trace", L="log")
@treturn String filename File name
@treturn String funcname Function name
@treturn Number lineno Line number
@treturn Integer Linked Probe-ID (conditional)
--]]
function Trace:whatis(id)
    if type(id) == "number" or type(id) == "userdata" then
        -- nothing
    else
        error("Argument #1 is not a valid probe-ID (address)")
    end

    local n, t = probe.all()
    for _, v in pairs(t) do
        local e = {
            ptr = v[1],
            kind = v[2],
            filename = v[3],
            funcname = v[4],
            line= v[5]
        }
        if e.ptr == id then
            local ctrl, cntr, ts, diff_p, diff_t, diff_n = probe.getdata(e.ptr)
            if diff_p then
                return e.kind, e.filename, e.funcname, e.line, diff_p
            else
                return e.kind, e.filename, e.funcname, e.line
            end
        end
    end
end

--[[--
set

Set ctrl bit-mask for a selection of probe(s) and optionally it's/their
counter and time-stamp values.

First argument can be either a table of size and selection as returned by
`sel` or just the selection as size is implicit in the selection's
base-type/class (table)

It can also be omitted, in which case the selection in `self` is assumed and
the rest of the arguments starts at second argument.

Central operation about method is operating on groups of probes, i.e.
selections. An instance of Trace can have a selection (from `CTOR` or
appended), therefore it can operate on `self`

@usage -- Class static
tps = Trace('test_main.c','thread_A')
Trace:set(tps, 1)
Trace:p_stat(tps)

@usage -- Object instances
tps = Trace('test_main.c','thread_A')
tps:set(1)
tps:p_stat()
tps:set(1, 0)  -- Continue counting, reset counters but not timestamps
tps:p_stat()

@see sel
@function set
@tparam[opt=self] Table s Trace instance or Trace-selection table
@tparam Integer ctrl Control (bit-mask). Required
@tparam[opt] Integer cntr Counter.
@tparam[optchain=0] Integer ts Time-stamp.
@treturn Integer Number of probes fitting selection
@treturn Integer Number of probes changed
--]]
function Trace:set(_s, ...)
    local t
    local s, ctrl, cntr, ts
    if type(_s) == "number" then
        s = self
        ctrl = _s
        cntr, ts = ...
    else
        s = _s
        ctrl, cntr, ts = ...
    end

    if  not type(s) == "table" then
        error("Argument #1 is not a valid selection (1)")
    end
    if s[2] and s[2].__name and s[2].__name == "tp.sel" then
        t = s[2]
    else
        t = s
    end

    if not ctrl then
        error("Argument #2 (ctrl) is required")
    end
    if not cntr then
        cntr = 0
    end
    if not ts then
        ts = 0
    end

    local j = 0
    for i, l in ipairs(t) do
        if (t[i].ptr) then
            j = j + 1
            print(j, t[i].ptr, t[i].kind, ctrl, cntr, ts)
            if not probe.setdata(t[i].ptr, ctrl, cntr, ts) then
                error("probe.setdata failure: " .. j .. " " .. t[i].ptr)
            end
        end
    end
    return j
end

--[[--
sel

Return a selection of probes in a table based a human-readable specifiers of
source-code references.

Arguments are used to define a "selection". A "selection" is one or a
group of probe-ID's having the search (or "selection") criteria in common.
All arguments are optional (varargs).

* No arguments returns all probes in a process.
* Arguments of `String`-type are matched using Lua regular expressions and
  don't need to be exact matches. This enables generalized scripting for
  variants of same or similar code.

Positional argument order matters. I.e. if any preceding arguments don't
need specifying (in this method meaning: "include all of what that arguments
specifier stands for"), it/they still have to be passed but with the value
`nil`.

Sub-selection argument(s) (optional):
* If only one is given: A 1-element sub-selection at that index of the
  overall selection is returned
* If both: The sub-selection is the range between the two indexes
* If first index greater than last: The sub-selection is the range between
  the two indexes, but in **reversed order**.

Return is number of probes fitting selection and corresponding table with
following key-values:

    ptr      (Integer) Address of probe used as unique ID
    kind     (String)  "T"=trace-probe, "L"=log-probe etc
    filename (String)  File-name
    funcname (String)  Function-name
    line     (Integer) Line-number

@usage -- Make a trial selection. No or all `nil` arguments returns all probes
Trace:sel()
-- 16      table: 0x7f83ecaca950

n, t = Trace:sel()
-- Repeat but this time assign the result, then print the first one
-- Note that the table returned is a 2D index-table of data-dictionaries
print(t[1].filename, t[1].funcname, t[1].line)
-- test_main.c     thread_A        56

@see utrace-probe.all
@function sel
@tparam[opt] Trace Instance with selection
@tparam String filename
@tparam String funcname
@tparam String kind
@tparam[opt] Integer idx1 Start of sub-selection
@tparam[optchain] Integer idx2 End of  sub-selection
@treturn Integer Number of matches for selection
@treturn Table {ID, kind, filename, funcname, line}
--]]
function Trace:sel(_t, ...)
    local t, n
    local is_dict = false
    local filename, funcname, kind, idx1, idx2
    if type(_t) == "table" then
        t = _t
        if n > 0 and t[1].ptr then
            is_dict = true
        end
        filename, funcname, kind, idx1, idx2 = ...
    else
        n, t = probe.all()
        filename, funcname, kind, idx1, idx2 = _t, ...
    end

    local r = Trace:new()
    local j = 0
    local curr = {}
    local ll
    local lx
    local new_series = false
    for _, l in ipairs(t) do
        local e
        if is_dict then
            e = l
        else
            e = {
                ptr = l[1],
                kind = l[2],
                filename = l[3],
                funcname = l[4],
                line= l[5]
            }
        end
        if not curr.ptr then
            curr = e
            new_series = true
            lx = 0
        else
            if curr.funcname == e.funcname then
                --
            else
                curr = e
                new_series = true
                ll =  -1;
                lx = 0
            end
        end

        if not filename or string.find(e.filename, filename) then
            if not funcname  or string.find(e.funcname, funcname) then
                if not kind  or string.find(e.kind, kind) then
                    if new_series then
                        table.insert(r, e)
                        new_series = false
                    else
                        -- Handle address-order != lineo-order
                        if e.line > ll then
                            -- Append to end
                            table.insert(r, e)
                        else
                            -- Insert first in new series
                            table.insert(r, #r - lx , e)
                        end
                        lx = lx + 1;
                    end

                    ll = e.line
                    j = j + 1
                end
            end
        end
    end

    if idx1 then
        local t = Trace(r):sub(idx1, idx2)
        t.__name = "tp.sel"
        return t
    end

    r.__name = "tp.sel"
    return r;
end

--[[--
sub

Sub-selection of instance-selection from index range, or singular probe
still as instance of Trace, for singular index.

@usage -- Sub-select functions from file-selection
m = Trace('.*main.c')
A = m:sub(1, 2)
B = m:sub(4)
R = m:sub(6, 5) -- reversed
print(A + B + R)
-- {
--   {106297695531040,  "T",  "test_main.c",  "thread_A",  57},
--   {106297695531104,  "T",  "test_main.c",  "thread_A",  59},
--   {106297695531232,  "T",  "test_main.c",  "thread_B",  71},
--   {106297695531360,  "T",  "test_main.c",  "main",  134},
--   {106297695531296,  "T",  "test_main.c",  "main",  131}
-- }

@usage -- Class-static usage
m = Trace('.*main.c')
D = Trace:sub(m, 2)
print(D)
-- {
--   {110968930517088,  "T",  "test_main.c",  "thread_A",  59}
-- }

@function sub
@tparam[opt=self] Trace s Use selection from Trace-instance
@tparam Integer idx1 Start of sub-selection
@tparam[opt] Integer idx2 End of sub-selection
@treturn Trace New instance with sub-selection
--]]
function Trace:sub(_s, ...)
    local idx1, idx2
    local s

    if type(_s) == "number" then
        s = self
        idx1 = _s
        idx2 = ...
    else
        s = _s
        idx1, idx2 = ...
    end

    if idx1 then
        if idx1 <= #s then
            do_sub = true
        else
            error("Sub-selection argument #4 out of range for selection")
        end
    end
    if idx2 then
        if idx2 <= #s then
            --
        else
            error("Sub-selection argument #5 out of range for selection")
        end
    end

    local inc
    if not idx2 then
        idx2 = idx1
    end

    if idx2 < idx1 then
        inc = -1
    else
        inc = 1
    end

    local subsel = Trace:new()
    local i
    for i = idx1, idx2, inc do
        table.insert(subsel, s[i])
    end

    return subsel;

end

--[[--
dprobe

Set up a low level probe-dependency using high-level code references

When the owner-probe is passed, a difference in both number of passed and
time is calculated. I.e. the second probe (the owner) also drives the event
to perform the diff-operation.

Function one or two Trace instance arguments, `dep` (dependent) and `own`
(owner). If one argument (dependent), the other (owner) is implied `self`.
Both instances is a symbolic selection containing exactly **one** probe
each.

@usage -- Using static class
a = Trace('test_main.c','thread_A',nil,1)
b = Trace('test_main.c','thread_A',nil,2)
Trace:set(a + b, 1) -- Enable both
Trace:dprobe(a, b)  -- Establish relation
Trace:p_stat(a + b) -- Print current status of both
-- 1       94660412502144  T       1       68615   3021884646707045
-- 2       94660412502208  T       1       68615   3021884651775924        5068879 0       94660412502144
-- 2

@usage -- Equivalent with instances
a = Trace('test_main.c','thread_A',nil,1)
b = Trace('test_main.c','thread_A',nil,2)
(a + b):set(1)   -- Enable both
a:dprobe(b)      -- Establish relation
(a + b):p_stat() -- Print current status of both
-- 1       94660412502144  T       1       68615   3021884646707045
-- 2       94660412502208  T       1       68615   3021884651775924        5068879 0       94660412502144
-- 2

@function dprobe
@tparam Trace dep Dependant probe
@tparam[opt=self] Trace own Owning probe
@treturn Boolean success
--]]
function Trace:dprobe(...)
    local err_arg = 0

    local own, dep
    local args = {...}

    if #args == 2 then
        own = args[2]
    else
        own = self
    end
    dep = args[1]

    if  type(own) == "table" and #own == 1 and own[1].ptr then
        --
    else
        err_arg = 1
    end

    if  type(dep) == "table" and #dep == 1 and dep[1].ptr then
        --
    else
        err_arg = 2
    end
    if not err_arg == 0 then
        error("Argument # ".. err_arg.." is not a valid Trace instance with exacly one selection")
        return false
    end

    x, y, x = probe.setdprobe(own[1].ptr, dep[1].ptr)
    return true
end

--[[--
new

Selection or copy/cast CTOR

* Return a new Trace object of selected probes. Identical to `sel`
  except that the return-type is a `Trace`-object for which methods can be
  applied
* If 1:st argument is a table, return a copy, optionally with sub-selected
  selection

@usage -- Instance of a selection
a = Trace('test_main.c','thread_A',nil,1)
b = Trace('test_main.c','thread_A',nil,2)
print(a + b)
-- {
--   {94660412502144,  "T",  "test_main.c",  "thread_A",  56},
--   {94660412502208,  "T",  "test_main.c",  "thread_A",  59}
-- }

@usage -- Instance of a sub-selection of selection
A = Trace('.*main.c', nil, nil, 1, 2)
print( A )
-- {
--   {106297695531040,  "T",  "test_main.c",  "thread_A",  57},
--   {106297695531104,  "T",  "test_main.c",  "thread_A",  59}
-- }

@usage -- First probe from all threads matching filenames
th = Trace('.*' , 'thread.*')
p  = Trace(th, '.*extra.*'):sub(1)
print(p)
-- {
--   {106297695531808,  "T",  "test_extra_thread.c",  "thread_C",  38}
-- }

@see sel
@function new
@tparam String arg1
@tparam String funcname
@tparam String kind
@tparam[opt] Integer idx1 Selection start index
@tparam[optchain] Integer idx2 Selection end index
@treturn Trace A Trace object instance
--]]
function Trace:new(arg1, ...)
    local s
    local t = Trace();

    if type(arg1) == "table" then
        args = {...}
        if #args > 0 then
            s = self:sel(arg1, ...);
        else
            s = arg1
        end
        for i,l in ipairs(s) do table.insert(t, l) end
        return t
    end

    if arg1 or funcname or kind then
        s = Trace:sel(arg1, ...);
    else
        s = {}
    end
    for i, l in ipairs(s) do
        if (l.ptr) then
            table.insert(t, s[i])
        end
    end
    return t
end

-- vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
