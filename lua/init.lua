--[[--
Lua VM start-up initialization example

Modify to fit

@script init.lua

@usage
export LUA_INIT="@$HOME/.lua/init.lua"

@usage
export LUA_INIT="@/var/lib/utrace-example/lua/init.lua"

@usage
export LUA_INIT="@$(pwd)/lua/init.lua"

--]]

--package.cpath = "/var/lib/utrace-example/lua/?.so;" .. package.cpath
--package.path = "/var/lib/utrace-example/lua/?.lua;" .. package.path
package.cpath = "lua/?.so;" .. package.cpath
package.path = "lua/?.lua;" .. package.path

-- Remove remark if existing & wanted
-- require("Log")
require("Trace")
