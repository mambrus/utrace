/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/*
 * Trace event and trace API
 */

#ifndef trace_h
#define trace_h

#include <utrace/utrace.h>
#include "utrace_config.h"
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>

struct trace_event {
    uint64_t time;               /* Recorded time-stamp (copy from probe) */
    const struct probe_meta *id; /* The probe assocoated with this record */
    pid_t pid;                   /* Recorded process- or thread- ID (system) */
    uint8_t core;                /* Which CPU core was running during the
                                    event */
};

int trace_stop(void);
int trace_start(void);
int trace_initialize(void);
unsigned int trace_create(unsigned int size);
unsigned int trace_save(const char *filename);
unsigned int trace_dump(const char *filename);

#endif // trace_h
