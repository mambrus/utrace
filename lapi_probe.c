/***
Lua API for a `utrace-probe`

`utrace` stands for uC-tracer and is a light-weight tracer for C which works
on everything from bare-silicon deeply embedded systems, to high-end OS:es
like Linux. Central part of `utrace` are it's probes which this module controls.

## See also

- [utrace](http://www.ambrus.se/~michael/ldoc/tplog/modules/utrace.html)

@see utrace-trace-rpc

@module utrace-probe
@author Michael Ambrus <michael@ambrus.se>
@copyright &copy; Michael Ambrus 2018-
@license MIT
*/
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <utrace/utrace.h>
#include <xb-lua/luabind.h>

#include "local.h"

#ifndef UNUSED
#    define UNUSED(X) ((void)(X))
#endif

/***
version string

@function version
@treturn string version
*/
static int l_version(lua_State *L)
{
    lua_pushstring(L, utrace_version());

    return 1;
}

/***
is module initialized

@function isinit
@treturn integer Balance, 0 if un-initialized.
*/
static int l_isinit(lua_State *L)
{
    lua_pushinteger(L, utrace_init_cnt());

    return 1;
}

/***
print all the probes unconditionally

@function listall
*/
static int l_listall(lua_State *L)
{
    UNUSED(L);
    probe_list_all();
    return 0;
}

/***
Get all probes into a table

Same as "listall" but instead of dumping to stdout, "all" returns a table and
the number of elements in it. This makes furter processing in Lua possible.

Each element in uprobe-table:
1. (intp)   ID          i.e. address of probe
2. (char)   kind        T=trace-probe, L=log-entry etc
3. (char*)  file-name
4. (char*)  function-name
5. (size_t) line-number

@usage -- Print a list similar to probe.listall()
n,up = probe.all()
for i=1,n do
    print(up[i][1], up[i][2], up[i][3], up[i][4], up[i][5])
end

@function all
@treturn integer number of probes
@treturn table uprobe-table
*/
static int l_all(lua_State *L)
{
    UNUSED(L);
    int nrow = 0;
    size_t nprobes = probe_ntot();
    struct probe_meta **uprobe = probe_start_address();
    char *rs;
    UNUSED(rs);
    char kind[5] = { 0 };

    lua_pushinteger(L, probe_ntot());
    if (nprobes == 0)
        return 1;

    lua_newtable(L);
    for (size_t i = 0; i < nprobes; i++) {
        lua_newtable(L);

        lua_pushinteger(L, (iptr)uprobe[i]);
        lua_rawseti(L, -2, 1);

        kind[0] = uprobe[i]->kind;
        rs = (char *)lua_pushstring(L, kind);
        assert(rs);
        lua_rawseti(L, -2, 2);

        rs = (char *)lua_pushstring(L, uprobe[i]->file);
        assert(rs); /* Confirm that the Lua-VM has accepted the string */
        lua_rawseti(L, -2, 3);

        rs = (char *)lua_pushstring(L, uprobe[i]->func);
        assert(rs); /* Confirm that the Lua-VM has accepted the string */
        lua_rawseti(L, -2, 4);

        lua_pushinteger(L, uprobe[i]->line);
        lua_rawseti(L, -2, 5);

        /* Finalize row in parent table */
        lua_rawseti(L, -2, 1 + nrow);
        nrow++;
    }
    return 2;
}

/***
total number of probes

@function total
@treturn integer number of probes
*/
static int l_total(lua_State *L)
{
    lua_pushinteger(L, probe_ntot());

    return 1;
}

/***
Probe meta info

@function meta
@treturn int start_adress
@treturn int stop_adress
@treturn int size of section in pytes
@treturn int size of a probe: sizeof(struct probe_meta)
*/
static int l_meta(lua_State *L)
{
    lua_pushinteger(L, (uintptr_t)probe_start_address());
    lua_pushinteger(L, (uintptr_t)probe_stop_address());
    lua_pushinteger(L, (uintptr_t)probe_stop_address() -
                           (uintptr_t)probe_start_address());
    lua_pushinteger(L, probe_sizeof());

    return 3;
}

/***
setbase

set base directory. Setting affects all print-outs

@function setbase
@tparam string base directory
*/
static int l_setbase(lua_State *L)
{
    const char *tstr = luaL_checkstring(L, 1);
    char *cbase = probe_getbase();

    if (cbase != NULL)
        free(cbase);

    cbase = strdup(tstr);
    probe_setbase(cbase);

    return 0;
}

/***
getbase

get base directory

@function getbase
@tparam string base directory
*/
static int l_getbase(lua_State *L)
{
    lua_pushstring(L, probe_getbase());

    return 1;
}

/***
setdata

set probe data

@function setdata
@tparam long probe_id
@tparam int status (bitmask)
@tparam int cnt hits
@treturn boolean success
*/
static int l_setdata(lua_State *L)
{
    int rc;
    bool set_cnt;
    unsigned long long cnt;

    uptr iaddr = luaL_checknumber(L, 1);
    struct probe_meta *id = (struct probe_meta *)iaddr;
    struct probe_data data = *id->data;

    data.status = luaL_checknumber(L, 2);
    cnt = luaL_checknumber(L, 3);
    set_cnt = luaL_checknumber(L, 4);

    if (set_cnt)
        data.cnt = cnt;

    rc = probe_set_data(id, &data);
    lua_pushinteger(L, rc);
    return 1;
}

/***
setdprobe

Set another dependent "diff-probe" to calculate deltas from. This one is
event trigger for delta-calculation. To reset/inhibit calculation, set to
NULL. Using this function resets deltas, but current ones are returned.

Time-wise the latter probe should be set up to drive calculation. Since
deltas and linked probe-ID's are owned, the argument order is reversed. I.e.
argument order implies ownership, not the event-order.

@function setdprobe
@tparam long probe_id To drive delta-event
@tparam long diff_p Calculate delta against this probe-ID (link-to)
@treturn uint64_t curr_dtime
@treturn uint64_t curr_dcnt
*/
static int l_setdprobe(lua_State *L)
{
    int rc;
    struct probe_data data = { 0 };
    unsigned long long diff_t;
    iptr diff_p;
    uint64_t diff_n;
    iptr iaddr;

    iaddr = luaL_checknumber(L, 1);
    struct probe_meta *id1 = (struct probe_meta *)iaddr;
    iaddr = luaL_checknumber(L, 2);
    struct probe_meta *id2 = (struct probe_meta *)iaddr;

    rc = probe_get_data(id1, &data);
    if (!rc)
        return 0;

    diff_p = (iptr)data.diff_p;
    diff_t = data.diff_t;
    diff_n = data.diff_n;
    data.diff_p = id2;
    data.diff_t = 0;
    data.diff_n = 0;

    rc = probe_set_data(id1, &data);
    if (!rc)
        return 0;

    lua_pushinteger(L, diff_t);
    lua_pushinteger(L, diff_n);
    if (diff_p) {
        lua_pushinteger(L, diff_p);
        return 3;
    }
    return 2;
}

/***
getdata

get probe data

@function getdata
@tparam long probe_id
@treturn Integer status
@treturn Integer cnt Number of times probe has been passed
@treturn unit64_t time-stamp last seen as active
@treturn Integer Probe-ID for self-propelled time delta (if any)
@treturn unit64_t Diff-time (if any)
*/
static int l_getdata(lua_State *L)
{
    int rc;
    struct probe_data data = { 0 };

    iptr iaddr = luaL_checknumber(L, 1);
    struct probe_meta *id = (struct probe_meta *)iaddr;

    rc = probe_get_data(id, &data);
    if (!rc)
        return 0;

    lua_pushinteger(L, data.status);
    lua_pushinteger(L, data.cnt);
    lua_pushinteger(L, data.time);

    if (data.diff_p) {
        lua_pushinteger(L, (uptr)data.diff_p);
        lua_pushinteger(L, data.diff_t);
        lua_pushinteger(L, data.diff_n);
        return 6;
    }

    return 3;
}

/* Registration part below */
/* ------------------------------------------------------------------------- */

static const struct luaL_Reg this_lib[] = {
    /* clang-format off */
    { "version", l_version },
    { "isinit", l_isinit },
    { "listall", l_listall },
    { "total", l_total },
    { "meta", l_meta },
    { "setbase", l_setbase },
    { "getbase", l_getbase },
    { "setdata", l_setdata },
    { "getdata", l_getdata },
    { "all", l_all },
    { "setdprobe", l_setdprobe },
    { NULL, NULL } /* sentinel */
    /* clang-format on */
};

/* Lua or LBM uses this to register the library in a VM */
static int do_reg(lua_State *L)
{
    luaL_newlib(L, this_lib);
    return 1;
}

/* LBM uses this to register the library in a VM */
void bind_probe_library(lua_State *L)
{
    luaL_requiref(L, "probe", do_reg, 1);
    lua_pop(L, 1);
}
