/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* Functions common to whole project  */

#include <liblog/assure.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <utrace/lapi.h>
#include <utrace/utrace.h>
#include <xb-lua/console.h>
#include <xb-lua/luabind.h>

#include "utrace_config.h"
#include "local.h"

#ifdef UTRACE_USE_RPC
#    include <pthread.h>
#    include <tcp-tap/clientserver.h>
#    include "rpc.h"
#endif

static int port = UTRACE_DEFAULT_PORT;
static char *ifname = UTRACE_DEFAULT_IFNAME;
static const char UTRACE_VERSION_STR[] = UTR_PROJ_NAME " <" UTR_VERSION ">";

const char *utrace_version()
{
    return UTRACE_VERSION_STR;
}

const char *utrace_service_hostname()
{
    return ifname;
}

/* Base port, +N is implicit of which service is used */
int utrace_service_port()
{
    return port;
}

#ifndef UTRACE_AUTOINIT
/* Stand-in when initfini.c is not auto-initializing */
static int init_cntr = 0;
int utrace_init_cnt()
{
    return init_cntr;
}
#endif

/*
 Register supported Lua language API's
*/
void utrace_register_lbms()
{
#ifdef HAS_XB_LUA
    ASSURE(lua_registerlibrary(bind_probe_library) > 0);
#    ifdef UTRACE_USE_RPC
    ASSURE(lua_registerlibrary(bind_trace_rpc_library) > 0);
#    endif
#endif
}

/*
 Start Lua VM and console(/rpc) services

 If initfini.c is not used (i.e.  __utrace_init), deliberately or omitted by
 runtime loader, this function or a customized equivalent function needs to
 be (implemented) and called for services to work.

 Note: Probes work without services but are passive without supervision.
 Use-cases for not using projects supervision-by-services can be lack of OS
 support for threads, TCP-IP etc (i.e. bare silicon targets).
*/
void utrace_services_start()
{
#ifdef UTRACE_USE_RPC
    ASSURE(start_service_rpc(port + SERVICE_IDX_RPC, ifname) == 0);
#endif

#ifdef HAS_XB_LUA
#    ifndef UTRACE_LUA_SINGLESESSION
    /* Several clients can attach simultaneously, one VM for each. On reattach, VM is
     * "forgetful" (but states in C-code are not) */
    ASSURE(luacon_vm_multisession(port + SERVICE_IDX_TERMINAL, ifname) == 0);
#    else

    /* Only one client can attach at a time. Stdio from main process is also
       re-routed but restored on detach. VM remembers states between
       connections, i.e. suitable for external scripting.

       There also exists certain debug benefits as non-forked BP's
     */
    ASSURE(luacon_vm_singlesession(port + SERVICE_IDX_TERMINAL, ifname,
                                   stateful) == 0);
#    endif
#endif //HAS_XB_LUA
#ifndef UTRACE_AUTOINIT
    /* Stand-in when initfini.c is not auto-initializing. Not strictly
     * speaking the same meaning as initalized doe not have to include services*/
    init_cntr++;
#endif
    log_info(
        "%s [%s]: uTrace probes console service started %d times at: %s:%d\n",
        UTR_PROJ_NAME, UTR_VERSION, utrace_init_cnt(), ifname,
        port + SERVICE_IDX_TERMINAL);
}
