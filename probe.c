/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <liblog/assure.h>
#include <utrace/utrace.h>
#include "utrace_config.h"
#include "local.h"
#ifdef UTRACE_USE_RPC
#    include <pthread.h>
#    include <tcp-tap/clientserver.h>
#    include "rpc.h"
#endif

#undef NDEBUG
#include <assert.h>

static char *base = NULL;
static int base_idx = -1; /* If positive value, skip this many chars */
extern struct probe_meta *__start_uprobes;
extern struct probe_meta *__stop_uprobes;
static struct probe_meta **uprobe = &__start_uprobes;
#define SECTION_SIZE \
    ((unsigned long)&__stop_uprobes - (unsigned long)&__start_uprobes)
#define NPROBES (SECTION_SIZE / sizeof(struct probe_meta *))

static const char *strp_filename(const char *fn)
{
    int i, len;

    if (base == NULL)
        return fn;

    if (base_idx > 0)
        return &fn[base_idx];

    /* Calc a new idx */
    len = strnlen(fn, PATH_MAX);
    for (i = 0; (i < len) && (base[i] == fn[i]); i++)
        ;

    base_idx = i;
    return &fn[base_idx];
}

/* Unconditionally list all probes */
void probe_list_all(void)
{
    for (size_t i = 0; i < NPROBES; i++) {
        printf("%p %c %s %s %d\n", uprobe[i], uprobe[i]->kind,
               strp_filename(uprobe[i]->file), uprobe[i]->func,
               uprobe[i]->line);
    }
}

int probe_ntot(void)
{
    return NPROBES;
}

void *probe_start_address(void)
{
    return &__start_uprobes;
}

void *probe_stop_address(void)
{
    return &__stop_uprobes;
}

int probe_sizeof(void)
{
    return sizeof(struct probe_meta);
}

void probe_setbase(char *newbase)
{
    base = newbase;
}

char *probe_getbase()
{
    return base;
}

/* probe-data section */

#ifndef UTRACE_USE_RPC
int probe_set_data(struct probe_meta *id, struct probe_data *data)
{
    data->run = id->data->run;

    memcpy(id->data, data, sizeof(struct probe_data));

    /* Simulate: no error (WIP TODO) */
    return 1;
}

int probe_get_data(struct probe_meta *id, struct probe_data *data)
{
    memcpy(data, id->data, sizeof(struct probe_data));

    /* Simulate: no error  (WIP TODO) */
    return 1;
}
#else

int probe_set_data(struct probe_meta *id, struct probe_data *data)
{
    ASSURE(pthread_once(&once_rpc_control, rpc_once) == 0);
    int sn, rn;
    struct rpc_c2s c2s;
    struct rrpc_c2s rc2s;

    c2s.rpc = PROBE_SET_DATA;
    c2s.probe.prid = (iptr)id;

    memcpy(&c2s.probe.set.data, data, sizeof(struct probe_data));

    sn = write(utrace_rpc, &c2s, sizeof(struct rpc_c2s));
    assert(sn == sizeof(struct rpc_c2s));
    rn = read(utrace_rpc, &rc2s, sizeof(struct rrpc_c2s));
    assert(rn == sizeof(struct rrpc_c2s));

    return rc2s.probe.rc;
}

int probe_get_data(struct probe_meta *id, struct probe_data *data)
{
    ASSURE(pthread_once(&once_rpc_control, rpc_once) == 0);
    int sn, rn;
    struct rpc_c2s c2s;
    struct rrpc_c2s rc2s;

    c2s.rpc = PROBE_GET_DATA;
    c2s.probe.prid = (iptr)id;

    sn = write(utrace_rpc, &c2s, sizeof(struct rpc_c2s));
    assert(sn == sizeof(struct rpc_c2s));
    rn = read(utrace_rpc, &rc2s, sizeof(struct rrpc_c2s));
    assert(rn == sizeof(struct rrpc_c2s));

    memcpy(data, &rc2s.probe.get.data, sizeof(struct probe_data));

    return rc2s.probe.rc;
}

#endif
