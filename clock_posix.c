/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova.se                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* Slow but accurate and portable clock-source. POSIX 1003.1b compliant
 * (IEEE Std 1003.1b-1993 Real-time Extension)
 *
 * Suitable for calibration of other clock-sources or for slow performance
 * analyse.
 */

#include <time.h>
#include <stdint.h>

uint64_t utrace_clock()
{
    struct timespec ts_now;

    clock_gettime(CLOCK_MONOTONIC, &ts_now);
    return (((uint64_t)ts_now.tv_sec) * 1.0e9) + (uint64_t)ts_now.tv_nsec;
}
