/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*
 * Definitions that should not be in public / installed header
 */
#ifndef utrace_local_h
#define utrace_local_h

/* Stringify */
#undef _XSTR
#undef STR
#define _STR(x) #x
#define STR(x)  _STR(x)

#include <stdint.h>
#include <utrace/utrace.h>
#include "utrace_config.h"

/* Following defines for services use only (i.e. use will depend of build) */
#define SERVICE_IDX_TERMINAL 0
#define SERVICE_IDX_RPC      1

#ifdef UTRACE_USE_RPC
extern pthread_once_t once_rpc_control; /* pthread once controle variable */
void rpc_once(void);                    /* RPC once function */
extern int utrace_rpc;                  /* RPC socket */
#endif

/* Portable integer types that can hold an address. Signed or not is only
  meaningful for address deltas */
typedef intptr_t iptr;
typedef uintptr_t uptr;

struct probe_meta;
struct probe_data;

/* Non-public (but visible) initialization handling */
int utrace_init_cnt();
void utrace_register_lbms();

/* Platform or build option specific "clock_gettime" */
uint64_t utrace_clock();

/* Probe API */
void probe_list_all(void);
void probe_setbase(char *newbase);
char *probe_getbase(void);
int probe_set_data(struct probe_meta *id, struct probe_data *data);
int probe_get_data(struct probe_meta *id, struct probe_data *data);

#endif // utrace_local_h
