/***
Lua API for a `utrace-trace-rpc`

Control recordings when utrace is used for event-recorder based tracing.

@see utrace-probe

@module utrace-trace-rpc
@author Michael Ambrus <michael@ambrus.se>
@copyright &copy; Michael Ambrus 2018-
@license MIT
*/
#include <assert.h>
#include <liblog/assure.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <utrace/utrace.h>
#include <xb-lua/lauxlib.h>
#include <xb-lua/luabind.h>
#include <xb-lua/lua.h>
#include <xb-lua/lualib.h>

#include "local.h"
#include "rpc.h"
#include "trace.h"

/***
Create or re-create a trace-buffer

If previous trace-buffer exists, it will be removed
If tracing is pending, it will first be stopped

@function create
@tparam int size in elements
@treturn int elements in buffer or 0 if fail
*/
static int l_create(lua_State *L)
{
    unsigned long ret = 0;
    unsigned nelem = luaL_checkinteger(L, 1);
    ASSURE(pthread_once(&once_rpc_control, rpc_once) == 0);
    {
        int sn, rn;
        struct rpc_c2s c2s;
        struct rrpc_c2s rc2s;

        c2s.rpc = TRACE_CREATE;
        c2s.trace.events.create.size = nelem;

        sn = write(utrace_rpc, &c2s, sizeof(struct rpc_c2s));
        ASSURE(sn == sizeof(struct rpc_c2s));
        rn = read(utrace_rpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(rn == sizeof(struct rrpc_c2s));

        ret = rc2s.trace.events.rc;
    }
    lua_pushinteger(L, ret);

    return 1;
}

/***
Initializes tracing and clears current buffer

Tracing will be ready, but not started.
Return value is success or failure, the reason thereof is in logs.

@function init
@treturn boolean success/fail
*/
static int l_init(lua_State *L)
{
    int ret = 0;
    ASSURE(pthread_once(&once_rpc_control, rpc_once) == 0);
    {
        int sn, rn;
        struct rpc_c2s c2s;
        struct rrpc_c2s rc2s;

        c2s.rpc = TRACE_INITIALIZE;

        sn = write(utrace_rpc, &c2s, sizeof(struct rpc_c2s));
        ASSURE(sn == sizeof(struct rpc_c2s));
        rn = read(utrace_rpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(rn == sizeof(struct rrpc_c2s));

        ret = rc2s.trace.cntrl.rc;
    }
    lua_pushinteger(L, ret);

    return 1;
}

/***
Starts tracing

@function start
@treturn boolean success/fail
*/
static int l_start(lua_State *L)
{
    int ret = 0;
    ASSURE(pthread_once(&once_rpc_control, rpc_once) == 0);
    {
        int sn, rn;
        struct rpc_c2s c2s;
        struct rrpc_c2s rc2s;

        c2s.rpc = TRACE_START;

        sn = write(utrace_rpc, &c2s, sizeof(struct rpc_c2s));
        ASSURE(sn == sizeof(struct rpc_c2s));
        rn = read(utrace_rpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(rn == sizeof(struct rrpc_c2s));

        ret = rc2s.trace.cntrl.rc;
    }
    lua_pushinteger(L, ret);

    return 1;
}

/***
Stops tracing

@function stop
@treturn boolean success/fail
*/
static int l_stop(lua_State *L)
{
    int ret = 0;
    ASSURE(pthread_once(&once_rpc_control, rpc_once) == 0);
    {
        int sn, rn;
        struct rpc_c2s c2s;
        struct rrpc_c2s rc2s;

        c2s.rpc = TRACE_STOP;

        sn = write(utrace_rpc, &c2s, sizeof(struct rpc_c2s));
        ASSURE(sn == sizeof(struct rpc_c2s));
        rn = read(utrace_rpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(rn == sizeof(struct rrpc_c2s));

        ret = rc2s.trace.cntrl.rc;
    }
    lua_pushinteger(L, ret);

    return 1;
}

/***
Stops tracing and saves trace-buffer to file

@function save
@tparam string file-name
@treturn int number of recorded events
*/
static int l_save(lua_State *L)
{
    unsigned long ret = 0;
    const char *filename = luaL_checkstring(L, 1);
    ASSURE(pthread_once(&once_rpc_control, rpc_once) == 0);
    {
        int sn, rn;
        struct rpc_c2s c2s;
        struct rrpc_c2s rc2s;

        c2s.rpc = TRACE_SAVE;
        strncpy(c2s.trace.events.save.filename, filename, UTRACE_PATHMAX);

        sn = write(utrace_rpc, &c2s, sizeof(struct rpc_c2s));
        ASSURE(sn == sizeof(struct rpc_c2s));
        rn = read(utrace_rpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(rn == sizeof(struct rrpc_c2s));

        ret = rc2s.trace.events.rc;
    }
    lua_pushinteger(L, ret);
    return 1;
}

/***
dumps existng exents allready in file it to stdout

This function is not RPC as file content *is* the IPC.

NOTE: state of tracing is not affcted. I.e. any ongoing will continue in
parallell. If so, note that dumping will affect the tracing.

@function dump
@tparam string file-name
@treturn int number of recorded events
*/
static int l_dump(lua_State *L)
{
    unsigned int ret;
    const char *filename = luaL_checkstring(L, 1);

    ret = trace_dump(filename);
    lua_pushinteger(L, ret);
    return 1;
}

/* Registration part below */
/* ------------------------------------------------------------------------- */

static const struct luaL_Reg this_lib[] = {
    { "create", l_create }, { "init", l_init }, { "start", l_start },
    { "stop", l_stop },     { "save", l_save }, { "dump", l_dump },
    { NULL, NULL } /* sentinel */
};

/* Lua or LBM uses this to register the library in a VM */
static int do_reg(lua_State *L)
{
    luaL_newlib(L, this_lib);
    return 1;
}

/* LBM uses this to register the library in a VM */
void bind_trace_rpc_library(lua_State *L)
{
    luaL_requiref(L, "trace", do_reg, 1);
    lua_pop(L, 1);
}
