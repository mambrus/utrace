/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* Fast but highly ARCHITECTURE DEPENDENT implementation

   Filename denotes architecture.

   PMU is ARM's nomenclature for Performance Montitor Unit

   Unit value: cycles

   NOTE: PMU may need enabling. This can only be done by a kernelmodule.
         I.e. PMU accessability and function is a pre-requisite for this
         function to work.
         Ref: http://zhiyisun.github.io/2016/03/02/How-to-Use-Performance-Monitor-Unit-(PMU)-of-64-bit-ARMv8-A-in-Linux.html
*/

#include <stdint.h>

uint64_t utrace_clock()
{
    uint32_t value;

    asm volatile("mrs %0, pmevcntr0_el0" : "=r"(value));

    return (uint64_t)value;
}
