--[[--
High-level documentation and examples of the `Trace` class

### Introduction

`Trace` is a helper-class to manage low-level `probes` and `tracing`

Both `probes` and `tracing` are part of the same Lua C-API low-level module
[utrace](https://gitlab.com/mambrus/utrace) but address different areas of
<i>"tracing"</i>.

#### tracing
Tracing is usually just a recorder of events, usually into memory but can be
directed to a pipe as well. The tracer-part is responsible for:

* Set-up. Meaning select which probes should produce events is the
  recording and possibly triggers for start/stop.
* Starting the recording. I.e. either by triggers or more commonly,
  interactively by a user (combinations of both exist)
* Stopping the recording. The opposite of starting, otherwise identical.
* Dumping. Mechanisms for getting recordings out. This can be a formatted
  text at console, a pipe that's constantly updated (not recommended for
  precision measures), or saving to file.

#### probes
This is an entity at a much lower level. It's what gathers the time-stamps
for the above and if in state "tracing" updates the trace-buffer. Time-stamp
together with ID of the probe (which the probe knows itself) constitutes an
`event" in the trace-recording above.

Probes can be used without tracing as well for example for the following:

* As probes know where in code they exist they can be used to control LOGs
  (see `tplog`)
* Probes include their own last-passed counter and time-stamp variables.
  Therefore a "sort-of" tracer can be built by polling these. That's usually
  not a very good tracer.
* But a variant of the same theme is "profiling", which can be done by
  measuring time between two known probes in code. As mentioned, each probe
  contains an event counter. Measuring propagation-time can hence be done
  quite easily using only probes <i>(use-case requires knowing that the code
  is not concurrent of-course)</i>.

Given the skill of the engineer performing the profiling-task, both
trace-recording and native-probe based technique can provide equally
accurate results for profiling bottle-necks. If the use-case is simple,
using probes only is often easier/faster to set-up.

### Examples

#### 1. Using the Trace-class statically:

* Activate probes
* Inspect what source references a selection identifies
* Inspect run-time statuses
* Inspect more in detail what a probe really is (i.e. represents in code,
  i.e. it's "kind")

``` lua
-- Specify a "selection" and print it's source-code references.
Trace:p_sel("test_main.c")
-- 1       94651015758592  T       test_main.c     main    132
-- 2       94651015758656  T       test_main.c     main    129
-- 3       94651015758464  T       test_main.c     thread_B        71
-- 4       94651015758528  T       test_main.c     thread_B        68
-- 5       94651015758336  T       test_main.c     thread_A        59
-- 6       94651015758400  T       test_main.c     thread_A        56
-- 6
-- >

-- Activate probes in that selection (last arg=1 is a bit-mask)
Trace:set(Trace:sel("test_main.c"), 1)
-- 1       94651015758592  T       1       0       0
-- 2       94651015758656  T       1       0       0
-- 3       94651015758464  T       1       0       0
-- 4       94651015758528  T       1       0       0
-- 5       94651015758336  T       1       0       0
-- 6       94651015758400  T       1       0       0
-- 6
-- >

-- Any probe being passed in execution?
Trace:p_stat("test_main.c")
-- 1       94651015758592  T       1       53      96256435629472
-- 2       94651015758656  T       1       53      96256467092610
-- 3       94651015758464  T       1       157     96256473305079
-- 4       94651015758528  T       1       158     96256483908750
-- 5       94651015758336  T       1       327     96256487002998
-- 6       94651015758400  T       1       327     96256481945309
-- 6
-- >
-- I.e. all seems alive

-- Execute a second time to confirm counter- and time-variables are changing as expected
Trace:p_stat("test_main.c")
-- 1       94651015758592  T       1       293     96271565226971
-- 2       94651015758656  T       1       292     96271533758668
-- 3       94651015758464  T       1       868     96271575965608
-- 4       94651015758528  T       1       868     96271565384547
-- 5       94651015758336  T       1       1804    96271577697915
-- 6       94651015758400  T       1       1805    96271582766414
-- 6
-- >

-- "What is" the last of the alive probe-IDs? Do confirm with the initial `p_sel`
Trace:whatis(94651015758400)
-- T       test_main.c     thread_A        56
-- >
```

#### 2 - Selection subsets and joins using Object instances

Example 2 shows how to set up a diff-capable probe `probe.setdprobe`
using two ID's. But using ID's are not symbolic, which makes the method less
suitable for scripting (used in function tests-frameworks for example).

The following facts makes it possible to improve so the diff-probe can be
set up symbolically, and with that also scripting symbolically yet
predictively:

* Only one Probe-ID in each side of the diff are needed
* Even if probe-ID's (which are addresses) change between builds and even
  runs, their order of them does not.

Hence the first part, subsets are born. By adding an additional argument to
selections, index one specific probe can be selected (i.e. "subset"). With
two, a range could be selected (TBD).

Joins work in the opposite direction. So far API's examples demonstrated
operating on all arguments needed for a selection. That makes it rater crude
as when used, usually more than one id really interested in is addressed.
Joins are adding to selections together using the `+` operator, resulting in
a third.

Additionally, API's that took complete specifiers as arguments can take
specifiers as either temporary or specific instances (i.e. objects) of
`Trace` variables as arguments. That makes it possible to write scripts both
setting-up and using the probes like this:

``` lua
-- For fun (demonstration) lets print them:
n, t_b = Trace:sel('test_main.c','thread_B')
n, t_a = Trace:sel('test_main.c','thread_A')
Trace:p_sel(t_a)
-- 1       93885596172416  T       test_main.c     thread_A        57
-- 2       93885596172480  T       test_main.c     thread_A        59
-- 2
Trace:p_sel(t_b)
-- 1       93885596172544  T       test_main.c     thread_B        69
-- 2       93885596172608  T       test_main.c     thread_B        71
-- 2
```

Either selection is to big for a diff-probe. But as we know the code and
know that the **first probe** for both functions is always lets create a
selection _of one_ for of each function and enable them both simultaneously:

``` lua
n, t_a = Trace:sel('test_main.c','thread_A', nil, 1) -- 1:st in func
n, t_b = Trace:sel('test_main.c','thread_A', nil, 2) -- 2:nd in func
Trace:set(t_a, 1) --
Trace:set(t_b, 1) -- Both now enabled
```

<i>Still quite quirky right?</i>

Selections are just Lua `tables` and are limited to the built in `table`
class. We can instead use instantiated `Trace`-objects with arguments for
defining a selection to `CTOR` and thereby can write much more reliable,
compact and script-able code.

Instead of the above selections used so far, examples continues using
objects, see `new` for more details.

Equivalent code using Trace objects and selection specifier to each CTOR:

``` lua
a = Trace('test_main.c','thread_A', nil, 1) -- 1:st in func
b = Trace('test_main.c','thread_A', nil, 2) -- 2:nd in func
```

For fun (demonstration) lets print them...
* using class-static method (legacy):
``` lua
Trace:p_sel(a + b)
-- 1       93885596172416  T       test_main.c     thread_A        57
-- 2       93885596172480  T       test_main.c     thread_A        59
-- 2
```

But the `Trace` class has support for this case stringed meaning we can pass
(even temporary variables) to Lua's `print`:

``` lua
print(a + b)
-- {
--   {93885596172416,  "T",  "test_main.c",  "thread_A",  57},
--   {93885596172480,  "T",  "test_main.c",  "thread_A",  59}
-- }
```

When run interactively, <b>the print()</b> (Lua >= `5.3`) is not necessary
which is very convenient for checking what selection an objects contains, either
specific object instances or temporary results from class operators (like `+`).

Lets enable and print statuses of a dynamic set of probes (ofc a 3rd
variable could be assigned if preferred). Make sure something is processing
before executing the last line.

``` lua
Trace:set(a + b, 1) -- Enable both
Trace:p_stat(a + b) -- Check statuses. Are probes alive?
-- 1       93885596172416  T       1       283621  3029498599603105
-- 2       93885596172480  T       1       283621  3029498594519429
-- 2
```

Having each selection separated in a variables is practical and simplifies
code for for example when setting up a high-level delta-probe using
`Trace:dprobe` (the primary use-case) but also for outputting their statuses
together using temporary joins:

``` lua
Trace:dprobe(a, b)  -- Probe "b" is the owner due to order of `cause-effect`
Trace:p_stat(a + b) -- We're probably only interested in the `effect` (probe "q") though
-- 1       93885596172416  T       1       312460  3029794304053961
-- 2       93885596172480  T       1       312460  3029794298853756        5228880 1       93885596172416
-- 2
```

##### Conclusion

What the above output shows:

* Time-difference between the two probes (using the `nS` default clock
  source) is `5.22` mS
* N-Count difference is `1`, the causality-order seems incorrect but is OK
* but only and due to a certain raciness when probes are first  activated.
  If the <b>value is constant</b> the setup is good. I.e. value of `0` is the
  best possible sanity check that both code and measuring method are sane.

##### Final touches

Last but not least, the above is more than we need for tests and to avoid
having parsing the output on host, we can use `Trace:stat` directly and only
print (or average) the deltas of interest. Here's a complete scriptlet to
get started for own scripts.

``` lua
a = Trace('test_main.c','thread_A', nil, 1) -- 1:st in func
b = Trace('test_main.c','thread_A', nil, 2) -- 2:nd in func
Trace:dprobe(a, b)
Trace:set(a + b, 1)
for i=1,10 do
   stats = Trace:stat(b + a)
   mS = stats[1][5] / 1e6;
   print("Thread-A processing time: " .. mS .. " mS")
   os.execute("sleep 1")
end
-- Thread-A processing time: 5.223834 mS
-- Thread-A processing time: 5.134089 mS
-- Thread-A processing time: 5.115541 mS
-- Thread-A processing time: 5.219049 mS
-- Thread-A processing time: 5.146403 mS
-- Thread-A processing time: 5.07557 mS
-- Thread-A processing time: 5.11987 mS
-- Thread-A processing time: 5.221702 mS
-- Thread-A processing time: 5.061521 mS
-- Thread-A processing time: 5.081845 mS
```

I.e. about <b>`~5.05`mS</b>. Time variance is due to load jitter and how
kernel is compiled. Values are very close to accurate (see`quality.lua`)

#### 3 - Wider or generalized selections using regular expressions

An alternative ,or complementing, to joining selections using Trace
instances `+` operator, is selecting using **Lua flavoured regular
expressions**. This for example allows less platform-dependent scripting, if
for example only a letter in a filename is different, but function(s) of
interest are more or less the same (at least the probes and their order
are).

The feature should be used with care and only used when the prerequisite
above is met. Which in high level code should be often however...

In this example we're going to address functions. All functions in **any
file** having <i>"stat"</i> **as part** of it's function-name:

``` lua
s = Trace('.*','.*thread.*') -- Trace instance "s" including a selection
print(s) -- Confirm the selection
-- {
--   {98693946908704,  "T",  "test_main.c",  "thread_A",  57},
--   {98693946908768,  "T",  "test_main.c",  "thread_A",  59},
--   {98693946908832,  "T",  "test_main.c",  "thread_B",  69},
--   {98693946908896,  "T",  "test_main.c",  "thread_B",  71},
--   {98693946909472,  "T",  "test_extra_thread.c",  "thread_C",  38},
--   {98693946909536,  "T",  "test_extra_thread.c",  "thread_C",  39},
--   {98693946909600,  "T",  "test_extra_thread.c",  "thread_C",  40},
--   {98693946909664,  "T",  "test_extra_thread.c",  "thread_C",  41},
--   {98693946909728,  "T",  "test_extra_thread.c",  "thread_C",  42},
--   {98693946909792,  "T",  "test_extra_thread.c",  "thread_C",  43},
--   {98693946909856,  "T",  "test_extra_thread.c",  "thread_C",  44},
--   {98693946909920,  "T",  "test_extra_thread.c",  "thread_C",  45},
--   {98693946909984,  "T",  "test_extra_thread.c",  "thread_C",  46},
--   {98693946910048,  "T",  "test_extra_thread.c",  "thread_C",  47}
-- }

Trace:set(s, 1) -- Enable all probes in the selection
-- Confirmation printout to stdout of which probes listed. Excluded here
-- Confirm they're enabled, counting and gathering time-stamps
Trace:p_stat(s)
-- 1       98693946908704  T       1       1553    51316638822988
-- 2       98693946908768  T       1       1553    51316643888233
-- 3       98693946908832  T       1       745     51316637567329
-- 4       98693946908896  T       1       744     51316627006204
-- 5       98693946909472  T       1       87542339        51316643991070
-- 6       98693946909536  T       1       87541997        51316644023402
-- 7       98693946909600  T       1       87541696        51316644044633
-- 8       98693946909664  T       1       87541379        51316644064573
-- 9       98693946909728  T       1       87541200        51316644083472
-- 10      98693946909792  T       1       87541151        51316644102201
-- 11      98693946909856  T       1       87541172        51316644120942
-- 12      98693946909920  T       1       87541190        51316644136813
-- 13      98693946909984  T       1       87541230        51316644156075
-- 14      98693946910048  T       1       87541275        51316644175716
-- 14

```
That regular expression happens to select all but 2 probes in our little
test program. We know which they are, so lest make sure they were never
enabled:

``` lua
m = Trace('.*','.*main.*')
print(m)
-- {
--   {98693946908960,  "T",  "test_main.c",  "main",  131},
--   {98693946909024,  "T",  "test_main.c",  "main",  134}
-- }
> Trace:p_stat(m)
-- 1       98693946908960  T       0       0       0
-- 2       98693946909024  T       0       0       0
-- 2
```

#### 4 - OO bonanza

As a `Trace` class can be an object instance, question is what such an
instance operates on compared to to class-static usage. Answer is
`selection`, either on instance variable or temporary variable. In this
example we go completely OO bananas. Note: OO or not is up to the user,
`Trace` supports both as when used purely static, a Lua-class is just
principally another name for a Lua-module.

``` lua
tps = Trace('.*','.*thread.*') -- Instance of selections in all functions
                               -- with the word "thread" in it
tpC = tps:sel('.*', '.*C', nil, 3, 2) -- From there, sub-select 2 in
                                      -- "thread_C in reversed order"
tpA = tps:sel('.*', '.*A')      -- Sub-select the only 2 in"thread_A"
tpB = tps:sel('.*', '.*B')      -- Sub-select the only 2 in"thread_A"

-- Sub-select diff-probes and bind each to a dependent
dpC  = tpC:sub(1)               -- Owner chose from last in chronology
dpC:dprobe(tpC:sub(2))          -- Dependent from first in chronology
dpA  = tpA:sub(2)
dpA:dprobe(tpA:sub(1))
dpB  = tpB:sub(2)
dpB:dprobe(tpB:sub(1))

(tpA + tpB + tpC):set(1)        -- Enable all
-- 1       110435580104736 T       1       0       0
-- 2       110435580104800 T       1       0       0
-- 3       110435580104864 T       1       0       0
-- 4       110435580104928 T       1       0       0
-- 5       110435580105632 T       1       0       0
-- 6       110435580105568 T       1       0       0
-- 6
--
(tpA + tpB + tpC):p_stat() -- Check if alive?
-- 1       94253991768192  T       1       262     366461956390400
-- 2       94253991768256  T       1       262     366461951329423 5094678 1       94253991768192
-- 3       94253991768320  T       1       125     366461940643121
-- 4       94253991768384  T       1       125     366461951341373 10698252        0       94253991768320
-- 5       94253991769088  T       1       28717331        366461957067801 51      149     94253991769024
-- 6       94253991769024  T       1       28717316        366461957087615
-- 6
```

We can see from the counter-diffs that `thread_C` is running much faster
than `thread_A` and `thread_B` as there is a diff of `149` counts. It's
constant however, so it's ok (another indication is the _dT_=`51nS` which is
insanely small but a good confirmation of how efficient `uTrace` probes
actually are - see also `quality.lua`)

Lets finish off with a variant of the mean-value loop from a previous
example.
``` lua
dT = {}
dT.A = 0
dT.B = 0
dT.C = 0
print("Harvesting diffs for averages over 10s...")
for i=1,10 do
   dT.A = dT.A + tpA:stat()[2][5] / 1e6;
   dT.B = dT.B + tpB:stat()[2][5] / 1e6;
   dT.C = dT.C + tpC:stat()[1][5] / 1e6;
   --print("Thread-A processing time: " .. mS .. " mS")
   os.execute("sleep 1")
end

print("Threads A, B, C probes dT time: ",dT.A/10, dT.B/10, dT.C/10, "(mS)")
-- Threads A, B, C probes dT time:         5.1123753       10.599117       3.37e-05        (mS)
```
Again, we see _dT.C_=`34nS` which is in the same neighbourhood as initially.
Take a look in the function `thread_C` in the file `test_extra_thread.c` to
see why :-)

For a runnable implementation of the above, see
[`examples/dprobes.lua`](http://www.ambrus.se/~michael/ldoc/utrace/scripts/dprobes.lua.html)

### Important note

<i>Note that these examples are for demonstrating the `Trace` class and the
underlying `uTrace` framework. <i>"Performance-architecture</i>" is a
delicate art and not for anyone to interpretive at will. The setups used
also need more refinement WRT:

* If several streams are evaluated in parallel the same probes are passed
  for all streams and results must be harvested taking that into account.
* For real cases using launching reliable scripts and executing over `RPC` for
  real CI test-framework purposes
* Measuring processing time says nothing about utilization as
  process is scheduled in/out from `running`-state multiple times and for
  several reasons, Linux time-sharing being one of them. It does however
  give a pretty good hint however. Using statistics and relative measures,
  pretty accurate figures can be calculated.
* Measuring on a faulty system when for example causality is inverted or
  threads are overlapping in the temporal domain can't be done using this
  method. Method is aiming <b>regressions in CI</b>, sanity tests or
  <b>optimizing aid</b> during development.

@see Trace
@see utrace-probe
@module examples-Trace
--]]

-- vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
