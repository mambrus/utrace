/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* Fast but highly ARCHITECTURE DEPENDENT implementation

   Filename denotes architecture.

   TSC is Intel's nomenclature for Time Stamp Counter

   Unit value: CPU freq cycles (temporal unit and fix or not depends on CPU)

   NOTE:
   ====
   #1: This is an extremely fast high-res clock but which might be unreliable
   or entity might temporal variant depending on CPU / SMP

   To see which relevant flags are supported by your CPU:

   grep flags /proc/cpuinfo | \
      cut -f2 -d":" | \
      sed -e 's/ /\n/g' | \
      grep tsc | \
      sort -u

   Google and find out if your options makes tsc trustworthy for your use-case

  #2:
  As of writing, rdtsc is not a privileged instruction for Linux (4.15.0-39-generic)
  I.e. no kernel patch is needed for x84_64
*/

#include <time.h>
#include <stdint.h>

uint64_t utrace_clock()
{
    uint64_t var;
    uint32_t hi, lo;

    __asm volatile("rdtsc" : "=a"(lo), "=d"(hi));

    var = ((uint64_t)hi << 32) | lo;
    return var;
}
