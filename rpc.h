/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/* rpc.h
 *
 * Definitions that should not be in public / installed headers (for now).
 * Separated from local.h as for certain cases rpc is not needed, i.e. where
 * calls can be done directly files concerning rpc including this will not
 * be part of the build.
 */

#ifndef utrace_rpc_h
#define utrace_rpc_h

#include <utrace/utrace.h>
#include "utrace_config.h"
#include "local.h"
#define BUFF_SZ        1017
#define UTRACE_PATHMAX (BUFF_SZ - 32)

struct probe_meta;
struct probe_data;

/* Identifier defines protocol, i.e. behaviour and data */
typedef enum {
    PROBE_GET_DATA,
    PROBE_SET_DATA,
    TRACE_STOP,
    TRACE_START,
    TRACE_INITIALIZE,
    TRACE_CREATE,
    TRACE_SAVE
} rpc_t;

/* Client to server payload */
struct rpc_c2s {
    rpc_t rpc;
    union {
        struct {
            iptr prid;
            union {
            } get;
            union {
                struct probe_data data;
            } set;
        } probe;
        union {
            struct {
                union {
                } start;
                union {
                } stop;
                union {
                } initialize;
            } cntrl;
            struct {
                union {
                    struct {
                        unsigned int size;
                    } create;
                    struct {
                        char filename[UTRACE_PATHMAX];
                    } save;
                };
            } events;
        } trace;
    };
};

/* Client to server response */
struct rrpc_c2s {
    rpc_t rpc;
    union {
        struct {
            iptr prid;
            int rc;
            union {
                struct probe_data data;
            } get;
            union {
            } set;
        } probe;
        union {
            struct {
                int rc;
                union {
                    struct {
                    } start;
                    struct {
                    } stop;
                    struct {
                    } initialize;
                };
            } cntrl;
            struct {
                unsigned int rc;
                union {
                    struct {
                    } create;
                    struct {
                    } save;
                };
            } events;
        } trace;
    };
};

/* Server to client payload */
struct rpc_s2c {
    rpc_t rpc;
};

/* Server to cient response */
struct rrpc_s2c {
    rpc_t rpc;
};

int start_service_rpc(int port, const char *ifname);

#endif // utrace_rpc_h
