/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <utrace/utrace.h>
#include <liblog/log.h>
#include "utrace_config.h"
#include "local.h"
#undef NDEBUG
#include <assert.h>
#ifdef UTRACE_USE_RPC
#    include "rpc.h"
#endif

#define __init __attribute__((constructor))
#define __fini __attribute__((destructor))

static int init_cntr = 0;
extern struct probe_meta *__start_uprobes;

int utrace_init_cnt()
{
    return init_cntr;
}

/* Module initializers/de-initializers. When used as library (who don't have
 * a natural entry/exit function) these are used to initialize
 * de-initialize. Use to set predefined/default states and cleanup.
 *
 * This will work with shared utraceraries as well as with static as they get
 * invoked by RTL load/unload, with or without C++ code (i.e. functions will
 * play nice with C++ normal ctors/dtors).
 *
 * Keep log in to at least once per new build-/run-environment assert that
 * the mechanism works.
 */

void __init __utrace_init(void)
{
    init_cntr++;

#ifdef ENABLE_INITFINI_SHOWEXEC
    struct probe_meta **uprobe = &__start_uprobes;
    log_info("%s [%s]: initializing for: %s\n", UTR_PROJ_NAME, UTR_VERSION, UTR_PROJ_NAME);
    log_info(UTR_PROJ_NAME ": Probe size in .text section is circa [%d] bytes\n",
             uprobe[0]->prsz);
#endif
#ifdef ENABLE_INITFINI_SHOWEXEC
    log_info(UTR_PROJ_NAME ": Register Lua API modules\n");
#endif
    utrace_register_lbms();
#ifdef UTRACE_AUTOSTART_SERVICES
#    ifdef ENABLE_INITFINI_SHOWEXEC
    log_info(UTR_PROJ_NAME ": Staring Lua-vm\n");
#    endif
    utrace_services_start();
#endif
#ifdef ENABLE_INITFINI_SHOWEXEC
    log_info(UTR_PROJ_NAME ": Auto initialization completed successfully\n");
#endif
}

void __fini __utrace_fini(void)
{
    init_cntr--;
#ifdef ENABLE_INITFINI_SHOWEXEC
    log_info("% [%s]: de-initializing for: %s\n", UTR_PROJ_NAME, UTR_VERSION,
             UTR_PROJ_NAME);
    fflush(NULL);
#endif
}
