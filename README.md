# utrace

**uC-tracer**

## Documentation
* [wiki](https://gitlab.com/mambrus/utrace/-/wikis/home)
* [ldoc](http://www.ambrus.se/~michael/ldoc/utrace/)
* How to trial-run without install/build:
  [Try easy](https://gitlab.com/mambrus/utrace/-/wikis/Try-via-docker)

## See also

`utrace` can not only be used as a tracer, but also to enhance loggers to
become trace-points and/or controllable post-build. For a real-file
use-case, see:

* [tplog](https://gitlab.com/mambrus/tplog) - a <i>"**T**race**P**robe enhanced Logger"</i>

## History and why

`utrace` began as a branch under
[`liblog:b-utrace`](https://gitlab.com/mambrus/liblog/-/tree/utrace?ref_type=heads)
which drew it's inspiration from the Android system logger (hence the concept of
log-level and the compatible macros). It was supposed to be a natural
continuation of `liblog`.

The main feature was the ability to control logs post-build/post-deploy, as
there is a trade-off between the use logs provides and the drawback with too
much logging.

`utrace` soon became a project of it's own as it's extremely light-weight
and like `xb-lua` has a natural aim at tiny uC-systems (bare silicon without
OS). But it turned out that having this main-feature embedded in a project
with vastly different aims was a real challenge. The reason is spelled
<i>"dependencies"</i> - in many forms, some of them listed below. It's very
easy to end up with either of these:

* Circular build dependencies (Who builds who)
* Circular run-time dependencies. lib depends on lib in a circular loop.
* Runtime dependencies controlling the probes. To control a probe, which
  is the back-bone of controlling also a log-point, access and permission to
  it's memory is needed. `ptrace` based solutions were discarded due to that
  `ptrace` puts an implicit requirement on capabilities on systems (all bare
  silicon OS-less are automatically excluded).
* Most SW-tracers are also unbearably
  slow compared to what `utrace` can provide and speed matters.
* Hence xb-lua and it's VM and
  console extension to the original Lua. But for that to work, a VM can't be
  generic, i.e. Lua's require can't dynamically link shared libraries that with
  symbol dependencies going in the wrong direction.
* Name collisions, both for API and for DSO's

Most of the above quirks be handled, but it's just way much work with for
comparably little gain. <i>(At least compared to this project...)</i>


