/*******************************************************************************
 *  TCP/IP based RPC service and protocol
 *  Copyright: Michael Ambrus, 2016
 ******************************************************************************/
#include <assert.h>
#include <errno.h>
#include <liblog/assure.h>
#include <liblog/log.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <tcp-tap/clientserver.h>
#include <unistd.h>

#include "local.h"
#include "trace.h"
#include "rpc.h"

#ifndef STR
#  undef _STR
#  define _STR(x) #x
#  define STR(x) _STR(x)
#endif

/* >>> Module global */
pthread_once_t once_rpc_control = PTHREAD_ONCE_INIT;
int utrace_rpc = -1;

void rpc_once(void)
{
    ASSURE((utrace_rpc = open_client(utrace_service_port() + SERVICE_IDX_RPC,
                                     "127.0.0.1")) >= 0);
};

/* <<< Module global */

int handle_rpc(int srpc, char *buf, int sz)
{
    int sn;
    struct rpc_c2s *c2s;
    struct rrpc_c2s rc2s;
    struct probe_data tpd;
    struct probe_data *dpd_p, *ipd_p;

    ASSURE(sz == sizeof(struct rpc_c2s));
    c2s = (struct rpc_c2s *)buf;
    rc2s.rpc = c2s->rpc;

    switch (c2s->rpc) {
    case PROBE_GET_DATA:
        ipd_p = ((struct probe_meta *)(c2s->probe.prid))->data;
        memcpy(&rc2s.probe.get.data, ipd_p, sizeof(struct probe_data));
        rc2s.probe.rc = 1;

        sn = write(srpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(sn == sizeof(struct rrpc_c2s));
        break;
    case PROBE_SET_DATA:
        ipd_p = &c2s->probe.set.data;
        dpd_p = ((struct probe_meta *)(c2s->probe.prid))->data;

        memcpy(&tpd, dpd_p, sizeof(struct probe_data));
        /* Note: Can be racy here FIXME */
        dpd_p->run = tpd.run;
        dpd_p->cnt = ipd_p->cnt;
        dpd_p->status = ipd_p->status;

        rc2s.probe.rc = 1;
        sn = write(srpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(sn == sizeof(struct rrpc_c2s));
        break;
    case TRACE_STOP:
        rc2s.trace.cntrl.rc = trace_stop();
        sn = write(srpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(sn == sizeof(struct rrpc_c2s));
        break;
    case TRACE_START:
        rc2s.trace.cntrl.rc = trace_start();
        sn = write(srpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(sn == sizeof(struct rrpc_c2s));
        break;
    case TRACE_INITIALIZE:
        rc2s.trace.cntrl.rc = trace_initialize();
        sn = write(srpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(sn == sizeof(struct rrpc_c2s));
        break;
    case TRACE_CREATE:
        rc2s.trace.events.rc = trace_create(c2s->trace.events.create.size);
        sn = write(srpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(sn == sizeof(struct rrpc_c2s));
        break;
    case TRACE_SAVE:
        rc2s.trace.events.rc = trace_save(c2s->trace.events.save.filename);
        sn = write(srpc, &rc2s, sizeof(struct rrpc_c2s));
        ASSURE(sn == sizeof(struct rrpc_c2s));
        break;
    default:
        LOGE("Invalid RPC call: %d\n", c2s->rpc);
        return 0;
    }

    return 1;
}

struct sockdef {
    int port;
    const char *ifname;
};

struct service {
    struct sockdef sockdef;
    pthread_t thread;
};

static void *connection_manager(void *inarg);
static void *rpc_service(void *inarg);

static void *connection_manager(void *inarg)
{
    int s, fd;
    pthread_t t;
    struct service *tservice = (struct service *)inarg;
    struct service service;

    /* Make deep-copy inarg to corr stack variable, then free original. This
     * prevents memory leaks if thread is killed either by accident or
     * intentionally. IOW: just a better management of memory overall */
    memcpy(&service, tservice, sizeof(struct service));
    free(tservice);
    tservice = NULL;

    LOGD("Connection manager RPC thread starts [if:port] : [%s:%d]\n",
         service.sockdef.ifname, service.sockdef.port);
    s = init_server(service.sockdef.port, service.sockdef.ifname);
    LOGD("Listening at socket (%d)\n", s);

    while (1) {
        ASSERT((fd = open_server(s)) >= 0);
        LOGD("Connection opened (%d)\n", fd);

        pthread_create(&t, NULL, rpc_service, (void *)((intptr_t)fd));
    }

    return NULL;
}

static void *rpc_service(void *inarg)
{
    int rn;
    int fd = (long)inarg;
    char buf[BUFF_SZ];

    LOGI("%s: Session [%d] connected\n", __func__, fd);

    for (rn = 1; rn > 0;) {
        rn = read(fd, buf, BUFF_SZ);
        if (rn > 0) {
            ASSURE(handle_rpc(fd, buf, rn));
        }
    }
    if (rn == 0) {
        LOGI("Session [%d] disconnected normally...\n", fd);
    } else {
        LOGE("Session read error detected: " __FILE__ " +" STR(__LINE__) " %s",
             strerror(errno));
        LOGE("Session [%d] now disconnecting.\n", fd);
    }
    close(fd); /* Release resource */

    return NULL;
}

int start_service_rpc(int port, const char *ifname)
{
    struct service *service;

    ASSURE((service = malloc(sizeof(struct service))));
    service->sockdef.port = port;
    service->sockdef.ifname = ifname;

    return pthread_create(&service->thread, NULL, connection_manager, service);
}
