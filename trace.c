/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <liblog/assure.h>
#include <utrace/utrace.h>
#include "utrace_config.h"
#include "local.h"
#include "trace.h"
#ifdef UTRACE_USE_RPC
#    include <assert.h>
#    include <pthread.h>
#    include <tcp-tap/clientserver.h>
#    include "rpc.h"
#endif

static struct trace_event *event_buffer = NULL;
static unsigned int in_idx = 0;
static unsigned int overflow = 0;
static unsigned int sz = 0;
static unsigned int dotrace = 0;

void utrace_probe_run(const struct probe_meta *p)
{
    /* Possible speed-optimization potential: Move do-trace conditions
       below outside function for faster disabled probes */

    /* If inactive probe, just return ASAP.Improvement potential: Move
       condition below outside function for (possibly) faster skipping
       disabled probes */
    if (!p->data->status)
        return;

    /* Early local snap-shot of time-stamp and counter */
    unsigned long long cnt; /* Number of times code passed probe */
    uint64_t time; /* Time-stamp last time passing active probe */
    cnt = p->data->cnt;
    time = utrace_clock(); /* Last passed by active probe */
    cnt++;                 /* Not racy */

    /* If trigger for delta-calculation, calculate deltas from the dependent */
    if (p->data->diff_p) {
        /*TODO: Investigate possibility to guard using semaphores or avoid
           updating if not same thread-ID (costs cycles) */
        struct probe_data *pd = p->data->diff_p->data;
        p->data->diff_t = time - pd->time;
        p->data->diff_n = cnt - pd->cnt;
    }

    /* Update probe-data with local snapshots time-stamp/counter */
    p->data->cnt = cnt;
    p->data->time = time;

    /* Probe is a trace-probe, continue. Otherwise done. */
    if (!(p->data->status & UTRACE_DO_TRACE))
        return;

        /* Example of conditional probing */
#ifdef NOTNOW
    if (p->data->cnt > 5)
        p->data->status &= UTRACE_DO_TRACE;
#endif //NOTNOW

    /* System do-trace query */
    if (!dotrace)
        return;

#ifdef UTRACE_PROBE_NOTMUTED
    printf("ProbeID 0x%p event logged nr: %llu @ %d\n", p->addr, p->data->cnt,
           in_idx);
#endif
    /* No need to check trace-buffer to save time. Rely on helper funcs to
     * do their job */
    struct trace_event *evt = &event_buffer[in_idx];

    evt->time = p->data->time; /* Use the probes TS to be consistent */
    evt->id = p;
#ifdef NOTNOW
    /* Too costly to find out. Skip for now Possibly make build conditional */
    evt->pid = 0;
    evt->core = 0;
#endif //NOTNOW
    in_idx++;
    in_idx %= sz;
}

int trace_stop()
{
    if (dotrace) {
        dotrace = 0;
        LOGI("Tracing stopped\n");
    }

    return 1;
};

int trace_start()
{
    if (event_buffer == NULL)
        return 0;

    LOGI("Starting tracing\n");

    dotrace = 1;
    return 1;
};

int trace_initialize()
{
    unsigned int num;
    trace_stop();

    if (event_buffer == NULL)
        return 0;

    for (num = 0; num < sz; num++, in_idx %= sz) {
        memset(&event_buffer[in_idx], 0, sizeof(struct trace_event));
        in_idx++;
    }

    in_idx = 0;
    overflow = 0;

    LOGI("trace-buffer initialized for [%d] events \n", sz);
    return 1;
};

unsigned int trace_create(unsigned int size)
{
    trace_stop();

    if (event_buffer != NULL) {
        if (sz == size) {
            LOGI(
                "trace-buffer exists with same size [%d] elements as requested."
                " Re-initializing only.\n",
                size);
            trace_initialize();
            goto returnok;
        }
        event_buffer = realloc(event_buffer, size * sizeof(struct trace_event));
        if (event_buffer == NULL) {
            LOGE("realloc() failed allocating [%d] elements\n", size);
            goto returnfail;
        }
        if (size <= in_idx)
            in_idx = 0;
        sz = size;
        trace_initialize();
    } else {
        event_buffer = calloc(size, sizeof(struct trace_event));
        if (event_buffer == NULL) {
            LOGE("realloc() failed allocating [%d] elements\n", size);
            goto returnfail;
        }
        sz = size;
    }

returnok:
    LOGI("trace-buffer created for [%d] elements\n", sz);
    in_idx = 0;
    return sz;
returnfail:
    return 0;
};

unsigned int trace_save(const char *filename)
{
    unsigned int num;
    int fd, cnt = 0;
    trace_stop();

    fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC, 0644);
    if (fd < 0) {
        LOGE("Failed opening file for writing: %s, errno: %d\n", filename,
             errno);
        return 0;
    }

    if (overflow) {
        for (num = 0; num < sz; num++, in_idx %= sz) {
            write(fd, &event_buffer[in_idx], sizeof(struct trace_event));
            in_idx++;
            cnt++;
        }
    } else {
        for (num = 0; num < in_idx; num++) {
            write(fd, &event_buffer[num], sizeof(struct trace_event));
            cnt++;
        }
    }

    close(fd);
    LOGI("trace-buffer saved to file: [%s:%d]\n", filename, cnt);
    return cnt;
};

/* Outputs clear-text trace-buffer to stdout */
unsigned int trace_dump(const char *filename)
{
    int fd, cnt = 0;
    struct trace_event evt;
    fd = open(filename, O_RDONLY | O_CLOEXEC);
    if (fd < 0) {
        LOGE("Failed opening file for reading: %s, errno: %d\n", filename,
             errno);
        return 0;
    }

    while (read(fd, &evt, sizeof(struct trace_event))) {
        printf("%llu %p %d %d\n", (long long unsigned int)evt.time, evt.id,
               evt.pid, evt.core);
        cnt++;
    }

    close(fd);
    LOGI("trace-buffer file dumped to stdout from file: [%s:%d]\n", filename,
         cnt);
    return cnt;
};
