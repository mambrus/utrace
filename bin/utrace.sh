#!/bin/bash

THIS_SCRIPT=$(basename $(readlink -f $0))
SCRIPT_DIR=$(dirname $(readlink -f $0))
THIS_DIR=$(pwd)

UTRACE_PORT=${UTRACE_PORT-"9000"}

# Test for one or two prerequisites. Either is enough to pass
function test_prerequisite() {
	if [ "X$(which $1)" == "X" ] && [ "X$(which $2)" == "X" ]; then
		echo "Prerequisite error: $1" >&2
		echo "  Prerequisite $1/$2 is not installed on your system" >&2
		echo "  Can't continue without dependency. Please install." >&2
		echo
		echo "For help, type: $THIS_SCRIPT -h" 1>&2
		echo
		exit 1
	fi
}

function utrace_help() {
			cat <<EOF
NAME
        $THIS_SCRIPT - TCP-TAP terminal

SYNOPSIS
        $THIS_SCRIPT [options]

DESCRIPTION
        $THIS_SCRIPT - Interact with utrace backend

EXAMPLES
        $THIS_SCRIPT -c probe.listall()

OPTIONS

    General options
        -h          Show this help and exit
        -c          Command
        -p          Port
		-t          Explicit tcp-term to use (full-path)

OPERATION

    $THIS_SCRIPT interact with utrace backend by passing argument
    of option c (i.e. command) to the Lua back-end "as is".

    Using ";" as delimiter, multiple commands can be sent.

	 utrace is modal, i.e. this utility can be used to extend the
	 built-in script-ability in shell.

	 To load external Lua-library: 'loadfile("filename.lua")'
	 Execute external Lua-script: 'dofile("filename.lua")'

    Note:
	 -----

	 Albeit utrace is modal, the Lua VM is not. Additional libraries needed
	 for operation is required to be passed togeather with each command.

    Caution:
	 --------

	 Utrace service is multi-sessioned. I.e. race vurnerable if several
	 clients operate simultanously.


DEPENDENCIES

    tcp-term - Terminal-emulator over TCP. Part of tcp-tap package
    expect   - Interactive scripting language


FILES:

    .utrace:
       A program linked and supervised with utrace produces this file
       in the same directory where program was started providing all
       the options needed for this utility except for Commands (-c).

ENVIRONMENT

    Name                          Current value

    UTRACE_PORT            (-p) : ${UTRACE_PORT}
    UTRACE_TCP_TERM        (-t) : ${UTRACE_TCP_TERM}


AUTHOR
        Written by Michael Ambrus <michael@helsinova.se>, 3 Dec 2018

EOF
}

while getopts hc:p:t: OPTION; do
	case $OPTION in
	h)
		if [ -t 1 ]; then
			utrace_help $0 | less -R
		else
			utrace_help $0
		fi
		exit 0
		;;
	c)
		UTRACE_CMD="${OPTARG}"
		;;
	p)
		UTRACE_PORT="${OPTARG}"
		;;
	t)
		UTRACE_TCP_TERM="${OPTARG}"
		;;
	?)
		echo "Syntax error: options" 1>&2
		echo "For help, type: $THIS_SCRIPT -h" 1>&2
		exit 2
		;;
	esac
done
shift $(($OPTIND - 1))

if [ $# -ne 0 ]; then
	echo "Syntax error:" \
		"$THIS_SCRIPT takes no argument" \
		"" 1>&2
	echo "For help, type: $DTFILES_SH_INFO -h" 1>&2
	exit 2
fi

test_prerequisite tcp-term tcp_term.sh
test_prerequisite expect

if [ -f ${THIS_DIR}/.utrace ]; then
	#If not, silently ignore
	source ${THIS_DIR}/.utrace
fi

# Set if needed, i.e. not set by option. Order matters, i.e. assuming
# suffixed variant is part of dev-§path.
UTRACE_TCP_TERM=${UTRACE_TCP_TERM:="$(which tcp_term.sh)"}
UTRACE_TCP_TERM=${UTRACE_TCP_TERM:="$(which tcp-term)"}


EXPECT='
	exp_internal 0
	log_user 0
	spawn '"${UTRACE_TCP_TERM}"' -p'"${UTRACE_PORT}"'

	# Sanity-check initial response
	set timeout 1
	expect {
		"Lua * Lua.org, PUC-Rio" {
		}
		timeout {
			puts stderr "ERROR: Can not connect to service or service invalid.\n"
			exit 1
		}

		failed             abort
	}

	# Send command after prompt is received
	set timeout 1
	expect {
		">" {
			send "'${UTRACE_CMD}'\r"
		}
		timeout {
			puts stderr "ERROR: Un-expected time-out.\n"
			exit 1
		}

		failed             abort
	}
	expect "'${UTRACE_CMD}'"

	# Wait until command finished or pty/tty will be truncated
	log_user 1
	set timeout -1
	expect {
		">" {
			send_user "\n"
			exit 0
		}

		failed             abort
	}
'
# Run script + clean traces of prompt and whites-only lines
expect -c "${EXPECT}" | sed -Ee 's/^>//' | sed -Ee '/^[[:space:]]*$/d'
