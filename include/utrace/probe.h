/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef probe_h
#define probe_h

/* Only gcc V>=12 and llvm has macro. Simulate something close */
#ifndef __FILE_NAME__
/* Make sure gcc folks hasn't hanged ther futur minds */
#    if __GNUC__ > 11
#        error gcc version >= 12 has __FILE_NAME__ built-in
#    endif
#    define __FILE_NAME__ \
        strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__
#endif

/* Default runner provided by libutrace */
struct probe_meta;
void utrace_probe_run(const struct probe_meta *);

/* Assignable probe
   This macro declares micro-probe which chan be RHS to an assignement
 */
#define ANYPROBE_R(T)                                                        \
    ({                                                                       \
        __label__ PBEGIN, PEND;                                              \
PBEGIN:;                                                                         \
        static struct probe_data probe_data = { .status = disabled,          \
                                                .cnt = 0,                    \
                                                .run = utrace_probe_run,     \
                                                .diff_p = NULL,              \
                                                .diff_t = 0,                 \
                                                .diff_n = 0 };               \
        static struct probe_meta probe_meta = { .addr = &&PBEGIN,            \
                                                .prsz = (&&PEND - &&PBEGIN), \
                                                .file = __FILE_NAME__,       \
                                                .func = __func__,            \
                                                .line = __LINE__,            \
                                                .data = &probe_data,         \
                                                .kind = T };                 \
        static const struct probe_meta *volatile probe_metap                 \
            __attribute__((section("uprobes"))) = &probe_meta;               \
        probe_meta.data->run(&probe_meta);                                   \
PEND:;                                                                           \
        &probe_meta;                                                         \
    })

#define UPROBE() ANYPROBE_R('T')
#define LPROBE() ANYPROBE_R('L')

#endif // probe_h
