/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef utrace_h
#define utrace_h

#include <stdint.h>
#include "probe.h"

/* Probe status masks */
#define UTRACE_DO_TRACE  0x01
#define UTRACE_LOG_CNTRL 0x02 /* Controls LOGX (overrides log-level) */
#define UTRACE_DO_LOG    0x04

/*
Truth table (pstatus)

T C L  symbolic
=====  ========
0 0 0  disabled
1 0 0  T__
0 1 0  _C_
1 1 0  TC_
0 0 1  __L
1 0 1  T_L
0 1 1  _CL
1 1 1  TCL
*/

/* clang-format off */
enum pstatus {
    disabled = 0,
    T__,
    _C_,
    TC_,
    __L,
    T_L,
    _CL,
    TCL
};
/* clang-format on */

struct probe_meta;

/* Run-time part of probe */
struct probe_data {
    enum pstatus status;
    unsigned long long cnt; /* Number of times code passed probe */
    uint64_t time; /* Time-stamp last time passing active probe */
    void (*run)(const struct probe_meta *p);
    struct probe_meta *diff_p; /* Diff-time against this probe if set */
    uint64_t diff_t;           /* Last diff-time against diff_p probe */
    unsigned long long diff_n; /* Last diff-count against diff_p probe */
};

/* Meta-data of probe */
struct probe_meta {
    void *addr; /* Adress to probe in code */
    int prsz;   /* Size of probe in code */
    const char *file;
    const char *func;
    int line;
    struct probe_data *data; /* Live data */
    char kind;               /* What kind of probe it is (T=Trace) */
    void *piggy;             /* Extensions can piggy-back here */
};

/* API */
const char *utrace_version();
const char *utrace_service_hostname();
int utrace_service_port();
void utrace_services_start();
int probe_ntot(void);
void *probe_start_address(void);
void *probe_stop_address(void);
int probe_sizeof(void);

#endif // utrace_h
