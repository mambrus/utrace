/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <pthread.h>
#include <strings.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <liblog/log.h>
#include <utrace/utrace.h>
#include "examples_config.h"

void *thread_C(void *inarg)
{
    while (1) {
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
#ifdef NOTNOW
        PRINTF("Executing: %s\n", __func__);
        usleep(EXAMPLE_BASE_DELAY_US);
#endif //NOTNOW
    }
    return NULL;
}
