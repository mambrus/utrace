/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <liblog/assure.h>
#include <liblog/log.h>
#include <limits.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <utrace/lapi.h>
#include <utrace/utrace.h>
#include <xb-lua/luabind.h>

#include "utrace_config.h"
#include "examples_config.h"

#ifndef STR
#    undef _STR
#    define _STR(x) #    x
#    define STR(x)  _STR(x)
#endif

/* Not public (implicit) */
int utrace_init_cnt();         /* Module initializer counter */
#define SERVICE_IDX_TERMINAL 0 /* xb-lua terminal port-offset */
#define SERVICE_IDX_RPC      1 /* RPC port-offset */

void *thread_C(void *inarg);

static void *thread_A(void *inarg)
{
    while (1) {
        PRINTF("Executing: %s\n", __func__);
        UPROBE();
        usleep(EXAMPLE_BASE_DELAY_US / 2);
        UPROBE();
        usleep(EXAMPLE_BASE_DELAY_US / 2);
    }
    return NULL;
}

static void *thread_B(void *inarg)
{
    while (1) {
        PRINTF("Executing: %s\n", __func__);
        UPROBE();
        usleep((EXAMPLE_BASE_DELAY_US * 2.1) / 2);
        UPROBE();
        usleep((EXAMPLE_BASE_DELAY_US * 2.1) / 2);
    }
    return NULL;
}

int main(int argc, char **argv)
{
    pthread_t t;
    int port_base = utrace_service_port();
#ifdef UTRACE_AUTOINIT
    if (!(utrace_init_cnt() > 0)) {
        fprintf(stderr, "uTrace has failed auto-initialization\n");
        fflush(stderr);
        exit(1);
    }
#else
    /* Spell it out for the sake of exemple. Otherwise better use
     * UTRACE_AUTOINIT for LBM registration */
    printf("Register Lua-API bindings\n");
    ASSURE(lua_registerlibrary(bind_probe_library) > 0);
#    ifdef UTRACE_USE_RPC
    ASSURE(lua_registerlibrary(bind_trace_rpc_library) > 0);
#    endif
#endif

#if !(defined(UTRACE_AUTOINIT) && defined(UTRACE_AUTOSTART_SERVICES))
    /* The recommended scenario and how package is set-up default when built
        as library is to auto register LBM's but not start service. This
        permits several packages to auto-register before Lua-VM is started.

        NOTE: order "Lua-VM last" is required

        (Normal applications would not test this. But this example (which
        doubles as a test) does so services are not started twice
     */
    printf("Starting services\n");
    utrace_services_start();
#endif

    printf("utrace-test: Built in directory: %s\n", STR(__DIR__));
    printf("xb-lua console is running. To attach, use:\n");
    printf("tcp-term -p %d\n", port_base + SERVICE_IDX_TERMINAL);
#ifdef UTRACE_USE_RPC
    printf("uTrace RPC TCP-based control responds at: %d\n",
           port_base + SERVICE_IDX_RPC);
#endif
#ifdef EXAMPLE_WAIT_FOR_USER
    printf("Press any char to continue\n");
    fflush(NULL);
    getchar();
#endif

    pthread_create(&t, NULL, thread_A, NULL);
    pthread_create(&t, NULL, thread_B, NULL);
    pthread_create(&t, NULL, thread_C, NULL);
    printf("Code is running... Please probe me!\n");
    fflush(NULL);

    while (1) {
        UPROBE();
        PRINTF("Executing %s\n", __func__);
        usleep(EXAMPLE_BASE_DELAY_US * 3.14);
        UPROBE();
        usleep(EXAMPLE_BASE_DELAY_US * 3.14);
    }
    return 0;
}
