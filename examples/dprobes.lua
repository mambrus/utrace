--[[--
==================
dprobes.lua
==================

  Author: Michael Ambrus, March 2025

This program demonstrates differential probes using high-level class `Trace`

Part 1 - Setup selections of probes:
-------------------------------------
- Coarse selections for a groups of probes to operate on
- Sub-select from this into sub-groups
- Sub-select from sub-groups diff-probes. This must be a selection of 1

Part 2 - Roles: assign diff-probe
-------------------------------------
- Sub-select from sub-groups diff-probes. For each diff-probe, this must be
  a sub-selection of 1

Example of using this script:
=============================
``` lua
-- Arg1: Number of iterations
-- Arg2: Period-time in mS
-- Arg3: Clock-ticks per second (optional)
--         POSIX nS i.e.:
--             1e9
--         For PMU/TSC, check first (in kHz)
--             sudo cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq
--             (1.867e9 on 2.8GHz CPU is reasonable)

-- Produce 20 results with 111 mS in-between
loadfile("dprobes.lua")(20, 111)
-- 3:rd argument defaulted to POSIX clock-source (1e9)
-- Enable all probes of interest
-- 1       94777659393024  T       1       0       0
-- 2       94777659392960  T       1       0       0
-- 3       94777659392256  T       1       0       0
-- 4       94777659392320  T       1       0       0
-- 5       94777659392128  T       1       0       0
-- 6       94777659392192  T       1       0       0
-- Harvesting [20] diffs/loop-freq for averages over 2220 mS
-- Threads A, B, C probes dT time  :       5.1225695       10.62159965     4.79e-05        (mS)
-- Threads A, B, C frequency probes:       10.237395       21.2387576      0.00011645      (mS)
-- Threads A, B, C frequency probes:       97.681099537529 47.083733372427 8587376.556462  (kHz)
```

@see Trace
@see examples-Trace
@script dprobes.lua
@author Michael Ambrus, November 2025
--]]

-- require("Trace")

function msleep(n)
    os.execute("sleep " .. tonumber(n/1000))
end

local niter, period_ms, per_sec = ...
if not per_sec then
    print("3:rd argument defaulted to POSIX clock-source (1e9)")
    per_sec = 1e9
end
local per_ms = per_sec / 1000;

-- Part 1
local tps = Trace('.*','.*thread.*')
local tpC = tps:sel('.*', '.*C', nil, 3, 2) -- Intentional reverse order
local tpA = tps:sel('.*', '.*A')
local tpB = tps:sel('.*', '.*B')

-- Part 2
-- Normal diff-probes
local dpC  = tpC:sub(1)                     -- Owner chose from last in chronology
dpC:dprobe(tpC:sub(2))                      -- Dependent from first in chronology
local dpA  = tpA:sub(2)
dpA:dprobe(tpA:sub(1))
local dpB  = tpB:sub(2)
dpB:dprobe(tpB:sub(1))
-- print("Diff probes", dpC + dpA + dpB)

-- Self-diff probes
local spC  = tpC:sub(2)
spC:dprobe(tpC:sub(2))
local spA  = tpA:sub(1)
spA:dprobe(tpA:sub(1))
local spB  = tpB:sub(1)
spB:dprobe(tpB:sub(1))
-- print("Diff probes", spC + spA + spB)

print("Enable all probes of interest")
Trace(tpC + tpB + tpA):set(1)

-- print("Current status C")
-- tpC:p_stat()
-- print("Current status A")
-- tpA:p_stat()
-- print("Current status B")
-- tpB:p_stat()

local dT = {}
dT.A = 0
dT.B = 0
dT.C = 0
local sT = {}
sT.A = 0
sT.B = 0
sT.C = 0
print("Harvesting [" .. niter .. "] diffs/loop-freq for averages over ".. niter * period_ms .. " mS")
for i=1, niter+1 do
   dT.A = dT.A + tpA:stat()[2][5] / per_ms;
   dT.B = dT.B + tpB:stat()[2][5] / per_ms;
   dT.C = dT.C + tpC:stat()[1][5] / per_ms;

   sT.A = sT.A + tpA:stat()[1][5] / per_ms;
   sT.B = sT.B + tpB:stat()[1][5] / per_ms;
   sT.C = sT.C + tpC:stat()[2][5] / per_ms;
   --print("Thread-A processing time: " .. mS .. " mS")
   msleep(period_ms)
end

print("Threads A, B, C probes dT time  : ",dT.A/niter, dT.B/niter, dT.C/niter, "(mS)")
print("Threads A, B, C frequency probes: ",sT.A/niter, sT.B/niter, sT.C/niter, "(mS)")
print("Threads A, B, C frequency probes: ",1e3/(sT.A/niter), 1e3/(sT.B/niter), 1e3/(sT.C/niter), "(kHz)")
