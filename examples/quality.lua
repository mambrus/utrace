--[[--
==================
quality.lua
==================

  Author: Michael Ambrus, November 2018

This program demonstrates UPROBE impact

How to measure impact - approach 1:
----------------------------------
- Make a tight C-loop with "known" periodicity
  - Ref EXAMPLE_BASE_DELAY_US build option
- Run this script as function (see Example below)
- Note counter result
- Add 10x PROBES in the same loop
- Repeat and compare results

    EXAMPLE_BASE_DELAY_US   NR-probes   cnt  ndiff
    =====================   =========   ===  =====
    1000                    1           948
    1000                    10          948  0
    1000                    100         948  0

    100                     1           6627
    100                     100         6615-6554 12/1000 !!?

### Conclusion #1:

* UPROBE impact in a 1mS loop is unmeasurable as no loop-counter increase from
  N=1..100 is 0.
* Linux user-land timeliness accuracy is rather crappy ((1000-948)/1000)=0.5%
* Timeliness accuracy is unrelated to UPROBE due to desult #1, i.e. UPROBE
  proves bad timeliness, not the other way around.

Impact analysis - approach 2:
----------------------------------
Another approach is to have nothing else but probes in a tight loop activated
for Ns using this script:

    N-probes  Nsec  hits
    ========  ====  ===========
    1         1     462794852
    1         10   4614237640
    10        1      60598736
    10        10    601515797
    10        100  6011744735
    100       1       2289051
    100       10     21685873
    100       100   217771654
    1000      1        140299
    1000      10      1408774
    1000      100    14134869

### Conclusion #2:

* Counter-value scales quite well with test-time. 10% off but repeatable for
  all cases. The pattern of 10% missing means something, but it's unclear
  what.
* Scaling with more probes is non-linear and makes no sense. Difference
  between each case {1,10,100,1000} is {6.6,26.0,14,8}. The jumping up and
  down in particular, makes no sense. At least from the probes perspective
  as probes don't change only because there's more or less of them. Only
  thing that makes sense to this is the Linux kernel that has pseudo rules
  for execution-time, i.e. process can be penalized if execution too is long
  in certain cases, but gain some of it back in others. Or cashing which
  nobody can predict in advance.
* As differences in probes or static code optimization is unlikely, the only
  way to estimate probes weight in the temporal domain is either:

* Take the *best case* and divide the number of hits with the time
  activated. My guess is that if time were shortened even more, the count
  ratio would be even greater /i.e. (1/500E6)/s, where x is in seconds.
  I.e. a probes "true" impact is 2.2nS (best-case)
* Disassembly the probe and count the instruction and multiply with clock
  frequency. This would give the most precise answer. BUT it would still
  be an estimate as cashing (i.e. instruction memory fetch where memory
  speed is included) can't be predicted.
* An educated guess however is that this value divide with the best-case
  can give very good estimate of the overall temporal weight including
  cashing as scheduler behaviour has several magnitudes higher impact on
  linearity than cashing.

I.e. 2.2 nS per probe on a Intel(R) Core(TM) i7-3770K CPU @ 3.50GHz, which
is pretty damned close to 0 and certainly much faster than any syscall.

This measurement is btw very close to logically disabled probes as
trace-buffer wasn't implemented at the time of measurement.

Discussion about binary patching techniques for disabling:
----------------------------------------------------------

This discussion applies for High-end systems only, i.e. not deeply-embedded
ones where `.text  is in `RO`-memory and where binary patching is impossible
(or at least very impractical to the extent where it disqualifies it's own
purpose or gain) regardless.

With the impact calculations above, binary patching for disabled probes
ftrace-based binary patching techniques with NOP:s isn't motivated. Probe
size in `.text` is 18 bytes. Even if NOP:s do not take any time in CPU,
there's still the matter of fetching them from memory, i.e. the i-cache case
would still apply. Worse case on the machine used for this analyze, assuming
each NOP is one instruction-cycle. Which on this architecture on average
takes 11 clocks:

(1/3.5GHz)*(11+18) = 8.3nS

I.e. Disabled by binary patching probe would take 4x longer than logically
disabled ones (worse case). Considering the probes can be optimized, it's
not worth the effort. On the other hand, for some architectures it might be
(better instruction compactness or probes made shorter). For binary patching
techniques, the reverse-to-ftrace method but-still-not-syscall/kernel-based
would be better.


Example of using this script:
=============================
``` lua
-- Arg1: Sleep seconds
-- Arg2: Clock-ticks per second
--         POSIX nS i.e.:
--             1e9
--         For PMU/TSC, check first (in kHz)
--             sudo cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq
--             (1.867e9 on 2.8GHz CPU is reasonable)

assert(loadfile("examples/quality.lua"))(5, 1e9)
```
@script quality.lua
@author Michael Ambrus, November 2018

--]]

function sleep(n)
  os.execute("sleep " .. tonumber(n))
end

local sec, per_sec=...

print()
print()
print()
local n,up = probe.all()
print("Using the following probe for test:")
print(up[n][1], up[n][2], up[n][3], up[n][4], up[n][5])
print()
print("Please press enter in shell where you started utrace-test from now...")

local probe_id=up[n][1]
print("Initial probe state for:", probe_id)
print(probe.getdata(probe_id))
print()

-- enable probe and get and print current state
probe.setdata(probe_id, 1, 0, 0)
sleep(1); -- Small pause needed for initial probe values to set
local e, c1, t1 = probe.getdata(probe_id)
print("Status:", e, c1, t1)

print("Sleep seconds:", sec)
sleep(sec)

-- disable probe and get and print current state
probe.setdata(probe_id, 0, 0, 0)
local e, c2, t2 = probe.getdata(probe_id)
print("status", e, c2, t2)

local dt = (t2 - t1)/per_sec;
print("dT (sec):", dt);
print("N (million):", (c2 - c1)/1e6);
print("loop freq (MHz):", (c2 - c1)/(1e6*dt));
